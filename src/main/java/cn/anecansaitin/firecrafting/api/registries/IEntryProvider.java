package cn.anecansaitin.firecrafting.api.registries;

import net.minecraft.resources.ResourceLocation;

public interface IEntryProvider {
    /**
     * 获取这个实例的注册名
     * @return 注册名
     */
    ResourceLocation getRegistryName();

    /**
     * 获取这个实例的名称或Path
     * @return 名称
     */
    default String getName() {
        return getRegistryName().getPath();
    }
}
