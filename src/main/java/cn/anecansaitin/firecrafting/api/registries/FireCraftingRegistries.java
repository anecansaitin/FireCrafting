package cn.anecansaitin.firecrafting.api.registries;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.serializer.IFireCraftingManagerSerializer;
import cn.anecansaitin.firecrafting.api.common.serializer.ITimedItemsSerializer;
import cn.anecansaitin.firecrafting.api.common.serializer.IWorldCraftingTaskSerializer;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;

public final class FireCraftingRegistries {
    public static IForgeRegistry<ITimedItemsSerializer<?>> TIMED_ITEMS_SERIALIZERS;
    public static IForgeRegistry<IWorldCraftingTaskSerializer<?>> WORLD_CRAFTING_TASK_SERIALIZER;
    public static IForgeRegistry<IFireCraftingManagerSerializer<?>> FIRE_CRAFTING_MANAGER_SERIALIZER;

    public static DeferredRegister<ITimedItemsSerializer<?>> createTimedItemsRegister(String modId) {
        return DeferredRegister.create(new ResourceLocation(FireCrafting.MOD_ID, "timed_items_serializers"), modId);
    }

    public static DeferredRegister<IWorldCraftingTaskSerializer<?>> createWorldCraftingTaskRegister(String modId) {
        return DeferredRegister.create(new ResourceLocation(FireCrafting.MOD_ID, "world_crafting_task_serializer"), modId);
    }

    public static DeferredRegister<IFireCraftingManagerSerializer<?>> createFireCraftingManagerRegister(String modId) {
        return DeferredRegister.create(new ResourceLocation(FireCrafting.MOD_ID, "fire_crafting_manager_serializer"), modId);
    }
}
