package cn.anecansaitin.firecrafting.api.common.damagesource;

import cn.anecansaitin.firecrafting.FireCrafting;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.damagesource.DamageType;

public class ModDamageType {
    public static final ResourceKey<DamageType> ELECTRIC_SHOCK = register("electric_shock");
    public static final ResourceKey<DamageType>  ITEM_IN_FIRE = register("item_in_fire");

    private static ResourceKey<DamageType> register(String name) {
        return ResourceKey.create(Registries.DAMAGE_TYPE, new ResourceLocation(FireCrafting.MOD_ID, name));
    }
}
