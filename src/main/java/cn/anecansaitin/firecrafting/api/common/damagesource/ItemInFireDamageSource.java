package cn.anecansaitin.firecrafting.api.common.damagesource;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.damagesource.DamageType;

public class ItemInFireDamageSource extends DamageSource {
    private final BlockPos pos;

    public ItemInFireDamageSource(Holder<DamageType> holder, BlockPos pos) {
        super(holder);
        this.pos = pos;
    }

    public BlockPos getPos() {
        return pos;
    }
}
