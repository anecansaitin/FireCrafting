package cn.anecansaitin.firecrafting.api.common.event;

import cn.anecansaitin.firecrafting.api.common.crafting.IFireCraftingManager;
import net.minecraftforge.eventbus.api.Event;
import org.jetbrains.annotations.NotNull;

public class CreateManagerEvent extends Event {
    private IFireCraftingManager manager;
    private final boolean catalyst;

    public CreateManagerEvent(boolean catalyst, IFireCraftingManager manager) {
        this.catalyst = catalyst;
        this.manager = manager;
    }

    public void setManager(@NotNull IFireCraftingManager manager) {
        this.manager = manager;
    }

    public IFireCraftingManager getManager() {
        return manager;
    }

    public boolean isCatalyst() {
        return catalyst;
    }
}
