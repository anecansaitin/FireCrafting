package cn.anecansaitin.firecrafting.api.common.event;

import cn.anecansaitin.firecrafting.api.common.crafting.ITimedItems;
import net.minecraftforge.eventbus.api.Event;

public class CreateTimedItemsEvent extends Event {
    private ITimedItems timedItems;
    private final boolean catalyst;

    public CreateTimedItemsEvent(boolean catalyst, ITimedItems timedItems) {
        this.catalyst = catalyst;
        this.timedItems = timedItems;
    }

    public boolean isCatalyst() {
        return catalyst;
    }

    public ITimedItems getTimedItems() {
        return timedItems;
    }

    public void setTimedItems(ITimedItems timedItems) {
        this.timedItems = timedItems;
    }
}
