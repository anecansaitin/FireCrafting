package cn.anecansaitin.firecrafting.api.common.event;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.Entity;
import net.minecraftforge.event.entity.EntityEvent;

public class ItemBurnedInFireEvent extends EntityEvent {
    private final BlockPos firePos;

    public ItemBurnedInFireEvent(Entity entity, BlockPos firePos) {
        super(entity);
        this.firePos = firePos;
    }

    public BlockPos getFirePos() {
        return firePos;
    }
}
