package cn.anecansaitin.firecrafting.api.common.crafting;

import cn.anecansaitin.firecrafting.common.registries.FireCraftingRecipeTypes;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.core.RegistryAccess;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.Container;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IFireRecipe extends Recipe<Container> {
    NonNullList<ItemStack> emptyList = NonNullList.create();
    String TYPE_ID = "fire_crafting";

    /**
     * 验证传入的物品是否匹配配方。
     * @param items 用于合成的物品
     * @param pos 触发配方的火焰坐标
     * @param world 触发配方的世界
     * @return 匹配返回true
     */
    boolean matches(List<ItemStack> items, BlockPos pos, ServerLevel world);

    /**
     * 存储了所有可适用配方的火焰。
     * @return 所有可用火焰
     */
    NonNullList<Block> getFires();

    /**
     * 带有获取配方所对应的世界中任务，当配方检测成功且将在世界中执行时被触发。
     * 比如生成物品、放置方块等。
     * <br/>
     * 无催化剂版本催化剂
     * @param items 触发配方的原料物品
     * @param pos 任务发生的坐标
     * @param world 任务发生的世界
     * @return 在世界中执行的任务
     */
    IWorldCraftingTask getWorldTask(List<ItemStack> items, BlockPos pos, ServerLevel world);

    /**
     * 带有获取配方所对应的世界中任务，当配方检测成功且将在世界中执行时被触发。
     * 比如生成物品、放置方块等。
     * <br>
     * 有催化剂版本
     * @param items 触发配方的原料物品
     * @param pos 任务发生的坐标
     * @param world 任务发生的世界
     * @param catalystIndex 催化剂在items中的位置
     * @return 在世界中执行的任务
     */
    IWorldCraftingTask getWorldTask(List<ItemStack> items, BlockPos pos, ServerLevel world, int catalystIndex);

    IMachineCraftingTask getMachineTask();

    /**
     * 描述这个配方，主要用于jei中。
     * @return 配方的描述信息
     */
    Component getDescription();

    @Deprecated
    @Override
    default boolean matches(@NotNull Container pContainer, @NotNull Level pLevel) {
        return false;
    }

    @Deprecated
    @Override
    default boolean canCraftInDimensions(int pWidth, int pHeight) {
        return false;
    }

    @Deprecated
    @Override
    default @NotNull ItemStack assemble(@NotNull Container container, @NotNull RegistryAccess registryAccess) {
        return ItemStack.EMPTY;
    }

    @Deprecated
    @Override
    default @NotNull ItemStack getResultItem(@NotNull RegistryAccess registryAccess) {
        return ItemStack.EMPTY;
    }

    @Override
    default @NotNull NonNullList<ItemStack> getRemainingItems(@NotNull Container pContainer) {
        return emptyList;
    }

    @Override
    default boolean isSpecial() {
        return true;
    }

    @Override
    default @NotNull RecipeType<?> getType() {
        return FireCraftingRecipeTypes.FIRE_CRAFTING.get();
    }
}
