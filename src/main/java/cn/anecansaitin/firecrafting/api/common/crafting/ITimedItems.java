package cn.anecansaitin.firecrafting.api.common.crafting;

import cn.anecansaitin.firecrafting.api.common.serializer.ITimedItemsSerializer;
import net.minecraft.world.item.ItemStack;

import java.util.List;

public interface ITimedItems {
    /**
     * 进行计时，每次触发将计时器前进一步。
     * <p>或者可以做任何其他的事情，比如修改存储的物品。
     */
    void tick();

    /**
     * @return 超时则返回True。
     */
    boolean isOvertime();

    /**
     * @return 物品保留的剩余时间
     */
    int getTime();

    /**
     * 设置倒计时时间为初始值
     */
    void setTime();

    /**
     * 设置倒计时的时间
     */
    void setTime(int time);

    /**
     * 添加物品到计时器中。
     * @param item 需要添加的物品。
     */
    void addItem(ItemStack item);

    /**
     * 添加物品到计时器中。
     * @param items 需要添加的物品。
     */
    void addItems(List<ItemStack> items);

    /**
     * @return 保存的所有物品。
     */
    List<ItemStack> getItems();

    /**
     * 当存储的物品被更新后返回True，用于在配方检测时避免相同物品重复判断。
     * @return 如果是物品被更新过，则返回True。
     */
    boolean isLatest();

    /**
     * 设置为已更新，每次物品变更时触发一次。
     */
    void setLatest();

    /**
     * 设置为已过期，每次配方检测过后触发一次。
     */
    void setExpired();

    /**
     * 和注册物品一样，序列化器需要使用{@link cn.anecansaitin.firecrafting.api.registries.FireCraftingRegistries TIMED_ITEMS_SERIALIZERS}
     * 创建一个DeferredRegister进行注册。
     * @return 序列化器
     */
    ITimedItemsSerializer<?> getSerializer();
}
