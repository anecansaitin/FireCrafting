package cn.anecansaitin.firecrafting.api.common.serializer;

import cn.anecansaitin.firecrafting.api.common.crafting.IWorldCraftingTask;
import cn.anecansaitin.firecrafting.api.registries.FireCraftingRegistries;
import cn.anecansaitin.firecrafting.api.registries.IEntryProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.core.registries.Registries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraftforge.server.ServerLifecycleHooks;

public interface IWorldCraftingTaskSerializer<T extends IWorldCraftingTask> extends IEntryProvider {
    Tag toNBT(T t);

    T fromNBT(Tag nbt);

    @Override
    default ResourceLocation getRegistryName(){
        return FireCraftingRegistries.WORLD_CRAFTING_TASK_SERIALIZER.getKey(this);
    }

    default void writeWorld(CompoundTag nbt, ServerLevel world) {
        nbt.putString("world", world.dimension().location().toString());
    }

    default ServerLevel readWorld(CompoundTag nbt) {
        return ServerLifecycleHooks.getCurrentServer().getLevel(ResourceKey.create(Registries.DIMENSION, new ResourceLocation(nbt.getString("world"))));
    }

    default void writePos(CompoundTag nbt, BlockPos pos) {
        nbt.putIntArray("pos", new int[]{pos.getX(), pos.getY(), pos.getZ()});
    }

    default BlockPos readPos(CompoundTag nbt) {
        int[] posNBT = nbt.getIntArray("pos");
        return new BlockPos(posNBT[0], posNBT[1], posNBT[2]);
    }
}
