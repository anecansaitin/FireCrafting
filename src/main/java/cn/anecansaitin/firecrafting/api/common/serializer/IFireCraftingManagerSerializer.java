package cn.anecansaitin.firecrafting.api.common.serializer;

import cn.anecansaitin.firecrafting.api.common.crafting.IFireCraftingManager;
import cn.anecansaitin.firecrafting.api.registries.FireCraftingRegistries;
import cn.anecansaitin.firecrafting.api.registries.IEntryProvider;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceLocation;

public interface IFireCraftingManagerSerializer<T extends IFireCraftingManager> extends IEntryProvider {
     Tag toNBT(T manager);

     T fromNBT(Tag nbt);

    @Override
    default ResourceLocation getRegistryName() {
        return FireCraftingRegistries.FIRE_CRAFTING_MANAGER_SERIALIZER.getKey(this);
    }
}
