package cn.anecansaitin.firecrafting.api.common.event;

import cn.anecansaitin.firecrafting.api.common.crafting.IWorldCraftingTask;
import net.minecraftforge.eventbus.api.Event;

public class WorldCraftingTaskExecutionEvent<T extends IWorldCraftingTask> extends Event {
    private final T task;

    public WorldCraftingTaskExecutionEvent(T task) {
        this.task = task;
    }

    public T getTask() {
        return task;
    }

    /**
     * 在任务执行前，无法确定本次任务的执行情况。
     */
    public static class Pre<T extends IWorldCraftingTask> extends WorldCraftingTaskExecutionEvent<T> {
        public Pre(T task) {
            super(task);
        }
    }

    /**
     * 在任务执行后，可以获取任务执行情况
     */
    public static class Post<T extends IWorldCraftingTask> extends WorldCraftingTaskExecutionEvent<T> {
        private final IWorldCraftingTask.TickResult taskResult;
        public Post(T task, IWorldCraftingTask.TickResult taskResult) {
            super(task);
            this.taskResult = taskResult;
        }

        public IWorldCraftingTask.TickResult getTaskResult() {
            return taskResult;
        }
    }
}
