package cn.anecansaitin.firecrafting.api.common.crafting;

import cn.anecansaitin.firecrafting.api.common.serializer.IWorldCraftingTaskSerializer;

public interface IWorldCraftingTask {
    /**
     * 任务类核心方法，每tick执行一次，可实现各种配方产物。
     */
    TickResult tick();

    /**
     * @return 若任务以全部完成，则返回True。
     */
    boolean isCompleted();

    /**
     * 如果任务被卡太久住或执行时间过久了，想要提前结束任务，可以让方法返回true，让manager删除它。
     * @return 是否超时
     */
    boolean isTimeout();

    /**
     * 和注册物品一样，序列化器需要使用{@link cn.anecansaitin.firecrafting.api.registries.FireCraftingRegistries WORLD_CRAFTING_TASK_SERIALIZER}
     * 创建一个DeferredRegister进行注册。
     * @return 序列化器
     */
    IWorldCraftingTaskSerializer<?> getSerializer();

    enum TickResult{
        /**
         * 任务成功执行，需要保存变化
         */
        DONE,
        /**
         * 任务没执行，且没有任何数据的变动，不需要保存变化
         */
        UNCHANGED,
        /**
         * 任务没执行，但任务内保存的数据出现了变动，需要保存变化
         */
        CHANGED
    }
}
