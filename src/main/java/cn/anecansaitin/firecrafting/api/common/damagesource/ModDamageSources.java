package cn.anecansaitin.firecrafting.api.common.damagesource;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;

import javax.annotation.Nullable;

public class ModDamageSources {
    public static DamageSource itemInFire(Level world, BlockPos pos) {
        Holder.Reference<DamageType> holder = world.registryAccess().registryOrThrow(Registries.DAMAGE_TYPE).getHolderOrThrow(ModDamageType.ITEM_IN_FIRE);
        return new ItemInFireDamageSource(holder, pos);
    }

    public static DamageSource electricShock(Level world) {
        return source(world, ModDamageType.ELECTRIC_SHOCK);
    }

    private static DamageSource source(Level world, ResourceKey<DamageType> damageType, @Nullable Entity from, @Nullable Entity to, @Nullable Vec3 pos) {
        Holder.Reference<DamageType> holder = world.registryAccess().registryOrThrow(Registries.DAMAGE_TYPE).getHolderOrThrow(damageType);
        return new DamageSource(holder, from, to, pos);
    }

    private static DamageSource source(Level world, ResourceKey<DamageType> damageType, @Nullable Entity from, @Nullable Entity to) {
        return source(world, damageType, from, to, null);
    }

    private static DamageSource source(Level world, ResourceKey<DamageType> damageType, @Nullable Entity from) {
        return source(world, damageType, from, null);
    }

    private static DamageSource source(Level world, ResourceKey<DamageType> damageType) {
        return source(world, damageType, (Entity) null);
    }

    private static DamageSource source(Level world, ResourceKey<DamageType> damageType, Vec3 pos) {
        return source(world, damageType, null, null, pos);
    }
}
