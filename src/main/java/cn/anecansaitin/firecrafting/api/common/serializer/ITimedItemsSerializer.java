package cn.anecansaitin.firecrafting.api.common.serializer;

import cn.anecansaitin.firecrafting.api.common.crafting.ITimedItems;
import cn.anecansaitin.firecrafting.api.registries.FireCraftingRegistries;
import cn.anecansaitin.firecrafting.api.registries.IEntryProvider;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceLocation;

public interface ITimedItemsSerializer<T extends ITimedItems> extends IEntryProvider {
    Tag toNBT(T t);

    T fromNBT(Tag nbt);

    @Override
    default ResourceLocation getRegistryName() {
        return FireCraftingRegistries.TIMED_ITEMS_SERIALIZERS.getKey(this);
    }
}
