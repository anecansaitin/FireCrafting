package cn.anecansaitin.firecrafting.api.common.crafting;

public interface IMachineCraftingTask {
    /**
     * 任务类核心方法，每tick执行一次，可实现各种配方产物。
     */
    void tick();

    /**
     *
     * @return 若任务以全部完成，则返回True。
     */
    boolean isCompleted();
}
