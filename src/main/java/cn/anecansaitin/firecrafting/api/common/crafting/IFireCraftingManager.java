package cn.anecansaitin.firecrafting.api.common.crafting;

import cn.anecansaitin.firecrafting.ModConfig;
import cn.anecansaitin.firecrafting.api.common.serializer.IFireCraftingManagerSerializer;
import cn.anecansaitin.firecrafting.common.event.FireCraftingHooks;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.ItemStack;

import java.util.*;

/**
 * 烈焰配方的世界合成核心处理类，每一tick执行一次
 */
public interface IFireCraftingManager {
    /**
     * 测试物品是否匹配某一个配方。
     *
     * @param items 输入的物品。
     * @param pos   物品对应的火焰坐标。
     * @param world 物品所在世界。
     * @return 匹配的配方。
     */
    IFireRecipe matchRecipe(List<ItemStack> items, BlockPos pos, ServerLevel world);

    boolean hasTimedItems(BlockPos pos);

    /**
     * @return 指定坐标的计时物品
     */
    ITimedItems getTimedItems(BlockPos pos);

    /**
     * @return 一个由坐标为键，被计时物品为值的Map。
     */
    Map<BlockPos, ITimedItems> getTimedItems();

    /**
     * 向坐标添加物品。
     * @param pos 烧毁物品火焰的坐标。
     * @param item 被烧毁的物品。
     */
    void addItem(BlockPos pos, ItemStack item);

    /**
     * 向坐标添加物品。
     * @param pos 烧毁物品火焰的坐标。
     * @param items 被烧毁的物品。
     */
    void addItems(BlockPos pos, List<ItemStack> items);

    /**
     * 移除坐标的物品。
     * @param pos 火焰坐标。
     */
    void removeTimedItems(BlockPos pos);

    /**
     * 执行指定的任务。
     * <br/>
     * 可以在任务执行前后做一些其他事情。
     * @param task 将被执行的任务
     * @return 任务执行结果
     */
    IWorldCraftingTask.TickResult performTask(IWorldCraftingTask task);

    /**
     * 将配方中的任务添加进执行列表内
     * @param recipe 检测成功的配方
     */
    void addTask(IFireRecipe recipe, List<ItemStack> items, BlockPos pos, ServerLevel world);

    /**
     * 用于保存各种配方的产物任务。
     * @return 所有可执行任务列表。
     */
    List<IWorldCraftingTask> getTasks();

    /**
     * 移除任务。
     * @param index 被移除的任务索引
     */
    void removeTask(int index);

    /**
     * 和注册物品一样，序列化器需要使用{@link cn.anecansaitin.firecrafting.api.registries.FireCraftingRegistries FIRE_CRAFTING_MANAGER_SERIALIZER}
     * 创建一个DeferredRegister进行注册。
     * @return 序列化器
     */
    IFireCraftingManagerSerializer<?> getSerialize();

    static ITimedItems createTimedItems() {
        return FireCraftingHooks.createTimedItems(ModConfig.CATALYST.get());
    }
}
