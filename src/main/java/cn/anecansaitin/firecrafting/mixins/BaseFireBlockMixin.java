package cn.anecansaitin.firecrafting.mixins;

import cn.anecansaitin.firecrafting.api.common.damagesource.ModDamageSources;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseFireBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

/**
 * 在火焰方块把物品烧掉时传入包含火焰坐标的伤害来源
 */
@Mixin(BaseFireBlock.class)
public abstract class BaseFireBlockMixin extends Block {
    @Shadow @Final private float fireDamage;

    public BaseFireBlockMixin(Properties properties) {
        super(properties);
    }

    @Inject(method = "entityInside", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/entity/Entity;hurt(Lnet/minecraft/world/damagesource/DamageSource;F)Z"), cancellable = true)
    public void mixin$entityInside(BlockState state, Level world, BlockPos pos, Entity entity, CallbackInfo ci) {
        if (entity instanceof ItemEntity) {
            entity.hurt(ModDamageSources.itemInFire(world, pos), this.fireDamage);
            ci.cancel();
        }
    }
}
