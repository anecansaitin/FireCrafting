package cn.anecansaitin.firecrafting.mixins;

import cn.anecansaitin.firecrafting.api.common.damagesource.ItemInFireDamageSource;
import cn.anecansaitin.firecrafting.common.event.FireCraftingHooks;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.level.Level;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

/**
 * 实现了当物品被火焰烧毁后触发物品被火烧毁事件。
 * 传入的伤害类型包含了烧毁物品的火焰位置
 */
@Mixin(ItemEntity.class)
public abstract class ItemEntityMixin extends Entity {
    protected ItemEntityMixin(EntityType<?> pEntityType, Level pLevel) {
        super(pEntityType, pLevel);
    }

    @Inject(method = "hurt", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/item/ItemStack;onDestroyed(Lnet/minecraft/world/entity/item/ItemEntity;Lnet/minecraft/world/damagesource/DamageSource;)V"))
    public void inject$hurt(DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
        if (source instanceof ItemInFireDamageSource s) {
            FireCraftingHooks.OnItemBurnedInFire(this, s.getPos());
        }
    }
}
