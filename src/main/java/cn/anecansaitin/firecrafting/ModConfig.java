package cn.anecansaitin.firecrafting;

import cn.anecansaitin.firecrafting.common.util.LoggerMarkers;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.ModLoadingContext;

public class ModConfig {
    public static final ForgeConfigSpec CONFIG;
    public static final ForgeConfigSpec.BooleanValue CATALYST;
    public static final ForgeConfigSpec.IntValue RETENTION_TIME;
    public static final ForgeConfigSpec.IntValue TASK_DEAD_LINE;

    static {
        ForgeConfigSpec.Builder builder = new ForgeConfigSpec.Builder();
        //Common开始
        builder.push("Craft in world config");
        //是否开启催化剂
        CATALYST = builder.comment("To start the catalyst mode, all the recipes within the world are only tested upon the addition of the catalyst, to avoid situations of recipe confusion.").define("Catalyst", true);
        //被火焰烧掉的物品的保存时间，超时删除
        RETENTION_TIME = builder.comment("The tick for storing burned items for recipe synthesis expires after which the items will be deleted. Adding new items will refresh the tick.").defineInRange("Retention time", 100, 20, Integer.MAX_VALUE);
        //当任务无法执行时，超过该时间就会将该任务删除
        TASK_DEAD_LINE = builder.comment("When a task cannot be executed, it will be deleted after that time.").defineInRange("Task dead line", 6000, 0, Integer.MAX_VALUE);
        //Common结束
        builder.pop();

        //完成
        CONFIG = builder.build();
    }

    public static void register() {
        FireCrafting.getLogger().info(LoggerMarkers.MOD_INITIALIZATION, "registerConfig");
        ModLoadingContext.get().registerConfig(net.minecraftforge.fml.config.ModConfig.Type.COMMON, CONFIG);
    }
}
