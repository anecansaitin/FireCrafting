package cn.anecansaitin.firecrafting.common.serializer;

import cn.anecansaitin.firecrafting.api.common.crafting.IFireCraftingManager;
import cn.anecansaitin.firecrafting.api.common.crafting.ITimedItems;
import cn.anecansaitin.firecrafting.api.common.crafting.IWorldCraftingTask;
import cn.anecansaitin.firecrafting.api.common.serializer.IFireCraftingManagerSerializer;
import cn.anecansaitin.firecrafting.api.common.serializer.ITimedItemsSerializer;
import cn.anecansaitin.firecrafting.api.common.serializer.IWorldCraftingTaskSerializer;
import cn.anecansaitin.firecrafting.api.registries.FireCraftingRegistries;
import cn.anecansaitin.firecrafting.api.registries.IEntryProvider;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceLocation;

public class SerializeHelper {
    @SuppressWarnings("unchecked")
    public static CompoundTag timedItemsNBT(ITimedItems timedItems) {
        CompoundTag root = new CompoundTag();
        ITimedItemsSerializer<ITimedItems> serializer = (ITimedItemsSerializer<ITimedItems>)timedItems.getSerializer();
        Tag nbt = serializer.toNBT(timedItems);
        saveType(root, serializer);
        root.put("item", nbt);
        return root;
    }

    @SuppressWarnings("unchecked")
    public static ITimedItems readTimedItems(CompoundTag nbt) {
        ITimedItemsSerializer<ITimedItems> serializer = (ITimedItemsSerializer<ITimedItems>) FireCraftingRegistries.TIMED_ITEMS_SERIALIZERS.getValue(analysisType(nbt));
        checkNull(serializer);
        return serializer.fromNBT(nbt.get("item"));
    }

    @SuppressWarnings("unchecked")
    public static CompoundTag worldCraftingTaskNBT(IWorldCraftingTask task) {
        CompoundTag root = new CompoundTag();
        IWorldCraftingTaskSerializer<IWorldCraftingTask> serializer = (IWorldCraftingTaskSerializer<IWorldCraftingTask>) task.getSerializer();
        Tag nbt = serializer.toNBT(task);
        saveType(root, serializer);
        root.put("task", nbt);
        return root;
    }

    @SuppressWarnings("unchecked")
    public static IWorldCraftingTask readWorldCraftingTask(CompoundTag nbt) {
        IWorldCraftingTaskSerializer<IWorldCraftingTask> serializer = (IWorldCraftingTaskSerializer<IWorldCraftingTask>) FireCraftingRegistries.WORLD_CRAFTING_TASK_SERIALIZER.getValue(analysisType(nbt));
        checkNull(serializer);
        return serializer.fromNBT(nbt.get("task"));
    }

    @SuppressWarnings("unchecked")
    public static CompoundTag fireCraftingManagerNBT(IFireCraftingManager manager) {
        CompoundTag root = new CompoundTag();
        IFireCraftingManagerSerializer<IFireCraftingManager> serializer = (IFireCraftingManagerSerializer<IFireCraftingManager>)manager.getSerialize();
        Tag nbt = serializer.toNBT(manager);
        saveType(root, serializer);
        root.put("manager", nbt);
        return root;
    }

    @SuppressWarnings("unchecked")
    public static IFireCraftingManager readFireCraftingManager(CompoundTag nbt) {
        IFireCraftingManagerSerializer<IFireCraftingManager> serializer = (IFireCraftingManagerSerializer<IFireCraftingManager>) FireCraftingRegistries.FIRE_CRAFTING_MANAGER_SERIALIZER.getValue(analysisType(nbt));
        checkNull(serializer);
        return serializer.fromNBT(nbt.get("manager"));
    }

    private static ResourceLocation analysisType(CompoundTag nbt) {
        if (!nbt.contains("type", Tag.TAG_STRING))
            throw new RuntimeException("Illegal NBT, String tag \"type\" not found.");

        return new ResourceLocation(nbt.getString("type"));
    }

    private static void saveType(CompoundTag nbt, IEntryProvider entry) {
        nbt.putString("type", getTypeString(entry));
    }

    private static void checkNull(Object o) {
        if (o == null)
            throw new RuntimeException("Illegal NBT, type not found.");
    }

    private static ResourceLocation getType(IEntryProvider entry) {
        return entry.getRegistryName();
    }

    private static String getTypeString(IEntryProvider entry) {
        return getType(entry).toString();
    }
}
