package cn.anecansaitin.firecrafting.common.world.saveddata;

import cn.anecansaitin.firecrafting.common.crafting.ManagerActuator;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.saveddata.SavedData;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class FireCraftingSavedData extends SavedData {
    private static final HashMap<DimensionType, FireCraftingSavedData> datum = new HashMap<>();
    private ManagerActuator actuator;

    public static FireCraftingSavedData getData(ServerLevel world) {
        return world.getDataStorage().computeIfAbsent(FireCraftingSavedData::read, () -> {
            ManagerActuator actuator = ManagerActuator.create();
            FireCraftingSavedData data = actuator.data();
            data.actuator = actuator;
            return data;
        }, "FireCrafting");
    }

    @Override
    public @NotNull CompoundTag save(@NotNull CompoundTag nbt) {
        return actuator.save(nbt);
    }

    private static FireCraftingSavedData read(CompoundTag nbt) {
        ManagerActuator actuator = ManagerActuator.read(nbt);
        FireCraftingSavedData data = actuator.data();
        data.actuator = actuator;
        return data;
    }

    public static ManagerActuator getActuator(ServerLevel world) {
        FireCraftingSavedData data = datum.get(world.dimensionType());
        ManagerActuator actuator;

        if (data == null) {
            FireCraftingSavedData data1 = getData(world);
            datum.put(world.dimensionType(), data1);
            actuator = data1.actuator;
        } else {
            actuator = data.actuator;
        }

        return actuator;
    }

    public static void cleanDatum() {
        datum.clear();
    }
}
