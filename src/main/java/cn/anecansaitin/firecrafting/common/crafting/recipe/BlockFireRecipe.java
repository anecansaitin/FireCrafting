package cn.anecansaitin.firecrafting.common.crafting.recipe;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.crafting.IMachineCraftingTask;
import cn.anecansaitin.firecrafting.api.common.crafting.IWorldCraftingTask;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingRecipeTypes;
import cn.anecansaitin.firecrafting.common.crafting.wordtask.BlockWorldTask;
import cn.anecansaitin.firecrafting.common.item.Catalyst;
import cn.anecansaitin.firecrafting.common.util.LoggerMarkers;
import com.google.gson.*;
import com.ibm.icu.impl.Pair;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.registries.ForgeRegistries;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class BlockFireRecipe extends AbstractFireRecipe {
    private final NonNullList<Pair<BlockPos, Block>> product;

    public BlockFireRecipe(ResourceLocation id, NonNullList<Ingredient> ingredients, NonNullList<Block> fires, Component description, NonNullList<Pair<BlockPos, Block>> product) {
        super(id, ingredients, fires, description);
        this.product = product;
    }

    @Override
    public IWorldCraftingTask getWorldTask(List<ItemStack> items, BlockPos pos, ServerLevel world) {
        return new BlockWorldTask(pos, world, product);
    }

    @Override
    public IWorldCraftingTask getWorldTask(List<ItemStack> items, BlockPos pos, ServerLevel world, int catalystIndex) {
        return new BlockWorldTask(pos, world, product, Catalyst.getOrientation(items.get(catalystIndex)));
    }

    @Override
    public IMachineCraftingTask getMachineTask() {
        return null;
    }

    //只能读取，不能修改内容
    public NonNullList<Pair<BlockPos, Block>> getProduct() {
        return product;
    }

    @Override
    public @NotNull RecipeSerializer<?> getSerializer() {
        return FireCraftingRecipeTypes.BLOCK_FIRE_SERIALIZER.get();
    }

    public static class Serializer extends AbstractRecipeSerializer<BlockFireRecipe> {
        public static final String NAME = FireCrafting.MOD_ID + ":block_fire_crafting";

        @Override
        public @NotNull BlockFireRecipe fromJson(@NotNull ResourceLocation id, @NotNull JsonObject jsonObject) {
            //原料
            NonNullList<Ingredient> ingredients = readIngredients(id, jsonObject);
            //火焰
            NonNullList<Block> fires = readFire(id, jsonObject);
            //描述
            MutableComponent description = Component.translatable(GsonHelper.getAsString(jsonObject, "description", ""));
            //产物方块
            HashMap<BlockPos, Block> productMap = new HashMap<>();
            JsonArray array = GsonHelper.getAsJsonArray(jsonObject, "product");

            for (JsonElement element : array) {
                JsonObject blockObject = element.getAsJsonObject();
                String blockName = GsonHelper.getAsString(blockObject, "block");
                Block block = ForgeRegistries.BLOCKS.getValue(new ResourceLocation(blockName));

                if (block == null)
                    throw new JsonParseException("Block does not exist. Recipe id: " + id);
                else if (block == Blocks.AIR)
                    continue;

                int[][] poss;

                if (!GsonHelper.isArrayNode(blockObject, "pos")) {
                    //没有pos或pos不是JsonArray则使用默认值
                    poss = new int[][]{{0, 0, 0}};
                    FireCrafting.getLogger().info(LoggerMarkers.RECIPE_SERIALIZER, "Block pos not define, use default [0,0,0]. Recipe id: " + id);
                } else {
                    JsonArray posArray = GsonHelper.getAsJsonArray(blockObject, "pos");

                    try {
                        if (posArray.get(0).isJsonArray()) {
                            //按照pos数组来读取
                            poss = new int[posArray.size()][3];

                            for (int j = 0; j < posArray.size(); j++) {
                                JsonArray p = posArray.get(j).getAsJsonArray();
                                poss[j] = new int[]{p.get(0).getAsInt(), p.get(1).getAsInt(), p.get(2).getAsInt()};
                            }
                        } else {
                            //按照单独pos读取
                            poss = new int[][]{{posArray.get(0).getAsInt(), posArray.get(1).getAsInt(), posArray.get(2).getAsInt()}};
                        }
                    } catch (UnsupportedOperationException exception) {
                        throw new JsonParseException("Pos must be a pos array or a single integer. Recipe id: " + id);
                    } catch (IndexOutOfBoundsException exception) {
                        throw new JsonParseException("Incomplete pos. Recipe id: " + id);
                    }
                }

                for (int[] ints : poss) {
                    //把所有pos拆成单个，并与方块组合
                    productMap.put(new BlockPos(ints[0], ints[1], ints[2]), block);
                }
            }

            NonNullList<Pair<BlockPos, Block>> product = NonNullList.createWithCapacity(productMap.size());

            for (Map.Entry<BlockPos, Block> entry : productMap.entrySet())
                product.add(Pair.of(entry.getKey(), entry.getValue()));
            //让方块按照从下往上的顺序排列
            product.sort(new PosComparator());
            return new BlockFireRecipe(id, ingredients, fires, description, product);
        }

        @Nullable
        @Override
        public BlockFireRecipe fromNetwork(@NotNull ResourceLocation id, @NotNull FriendlyByteBuf buf) {
            //原料
            NonNullList<Ingredient> ingredients = readIngredients(buf);
            //火焰
            NonNullList<Block> fire = readFire(buf);
            //描述
            Component description = buf.readComponent();
            //产物
            Pair<BlockPos, Block> pair = Pair.of(BlockPos.ZERO, Blocks.AIR);
            NonNullList<Pair<BlockPos, Block>> product = NonNullList.withSize(buf.readVarInt(), pair);

            for (int i = 0; i < product.size(); i++) {
                int[] ints = buf.readVarIntArray();
                product.set(i, Pair.of(new BlockPos(ints[0], ints[1], ints[2]), ForgeRegistries.BLOCKS.getValue(buf.readResourceLocation())));
            }

            return new BlockFireRecipe(id, ingredients, fire, description, product);
        }

        @Override
        public void toNetwork(@NotNull FriendlyByteBuf buf, @NotNull BlockFireRecipe recipe) {
            //原料
            writeIngredients(buf, recipe);
            //火焰
            writeFire(buf, recipe);
            //描述
            buf.writeComponent(recipe.description);
            //产物
            NonNullList<Pair<BlockPos, Block>> product = recipe.product;
            buf.writeVarInt(product.size());

            for (Pair<BlockPos, Block> pair : product) {
                BlockPos pos = pair.first;
                buf.writeVarIntArray(new int[]{pos.getX(), pos.getY(), pos.getZ()});
                buf.writeResourceLocation(ForgeRegistries.BLOCKS.getKey(pair.second));
            }
        }

        private static class PosComparator implements Comparator<Pair<BlockPos, Block>> {
            @Override
            public int compare(Pair<BlockPos, Block> pair1, Pair<BlockPos, Block> pair2) {
                if (pair1.first.getY() != pair2.first.getY()) {
                    return Integer.compare(pair1.first.getY(), pair2.first.getY());
                } else if (pair1.first.getX() != pair2.first.getX()) {
                    return Integer.compare(pair1.first.getX(), pair2.first.getX());
                } else {
                    return Integer.compare(pair1.first.getZ(), pair2.first.getZ());
                }
            }
        }
    }
}
