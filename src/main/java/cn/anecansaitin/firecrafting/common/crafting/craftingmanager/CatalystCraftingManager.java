package cn.anecansaitin.firecrafting.common.crafting.craftingmanager;

import cn.anecansaitin.firecrafting.api.common.crafting.ITimedItems;
import cn.anecansaitin.firecrafting.api.common.crafting.IFireRecipe;
import cn.anecansaitin.firecrafting.api.common.crafting.IWorldCraftingTask;
import cn.anecansaitin.firecrafting.api.common.serializer.IFireCraftingManagerSerializer;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingRecipeTypes;
import cn.anecansaitin.firecrafting.common.item.Catalyst;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingSerializers;
import cn.anecansaitin.firecrafting.common.serializer.SerializeHelper;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CatalystCraftingManager extends FireCraftingManager {
    @Override
    public IFireRecipe matchRecipe(List<ItemStack> items, BlockPos pos, ServerLevel world) {
        List<IFireRecipe> recipes = world.getRecipeManager().getAllRecipesFor(FireCraftingRecipeTypes.FIRE_CRAFTING.get());
        IFireRecipe result = null;

        //根据物品的数量添加物品
        ArrayList<ItemStack> list = new ArrayList<>();

        //去除最后一个物品，默认该物品为催化剂
        for (int j = 0, itemsSize = items.size(); j < itemsSize - 1; j++) {
            ItemStack item = items.get(j);
            for (int i = 0; i < item.getCount(); i++)
                list.add(item);
        }

        for (IFireRecipe recipe : recipes)
            if (recipe.matches(list, pos, world))
                result = recipe;

        return result;
    }

    @Override
    public void addTask(IFireRecipe recipe, List<ItemStack> items, BlockPos pos, ServerLevel world) {
        ItemStack catalyst = items.get(items.size() - 1);

        if (!(catalyst.getItem() instanceof Catalyst)) {
            throw new RuntimeException("Execution error, it will only reach here if the last item is a catalyst.");
        }

        addTask(recipe.getWorldTask(items, pos, world, items.size() - 1));
    }

    @Override
    public IFireCraftingManagerSerializer<?> getSerialize() {
        return FireCraftingSerializers.CATALYST_CRAFTING_MANAGER.get();
    }

    public static final class Serializer implements IFireCraftingManagerSerializer<CatalystCraftingManager> {
        @Override
        public Tag toNBT(CatalystCraftingManager manager) {
            CompoundTag root = new CompoundTag();
            Map<BlockPos, ITimedItems> items = manager.getTimedItems();
            //存物品
            if (items.size() > 0) {
                ListTag itemsNBT = new ListTag();
                root.put("items", itemsNBT);

                for (Map.Entry<BlockPos, ITimedItems> entry : items.entrySet()) {
                    CompoundTag node = new CompoundTag();
                    itemsNBT.add(node);
                    //存坐标
                    BlockPos pos = entry.getKey();
                    node.putIntArray("pos", new int[]{pos.getX(), pos.getY(), pos.getZ()});
                    //存物品
                    ITimedItems timedItems = entry.getValue();
                    node.put("items", SerializeHelper.timedItemsNBT(timedItems));
                }
            }

            //存任务
            List<IWorldCraftingTask> tasks = manager.getTasks();

            if (tasks.size() > 0) {
                ListTag tasksNBT = new ListTag();
                root.put("tasks", tasksNBT);

                for (IWorldCraftingTask task : tasks)
                    tasksNBT.add(SerializeHelper.worldCraftingTaskNBT(task));
            }

            return root;
        }

        @Override
        public CatalystCraftingManager fromNBT(Tag nbt) {
            if (nbt.getId() != Tag.TAG_COMPOUND)
                throw new RuntimeException("Illegal NBT. Require compound tag.");

            CatalystCraftingManager manager = new CatalystCraftingManager();
            CompoundTag compoundNBT = (CompoundTag) nbt;
            //取物品
            if (compoundNBT.contains("items", Tag.TAG_LIST)) {
                for (Tag item : compoundNBT.getList("items", Tag.TAG_COMPOUND)) {
                    int[] pos = ((CompoundTag) item).getIntArray("pos");
                    ITimedItems timedItems = SerializeHelper.readTimedItems(((CompoundTag) item).getCompound("items"));
                    manager.putTimedItems(new BlockPos(pos[0], pos[1], pos[2]), timedItems);
                }
            }
            //取任务
            if (compoundNBT.contains("tasks", Tag.TAG_LIST)) {
                for (Tag task : compoundNBT.getList("tasks", Tag.TAG_COMPOUND)) {
                    manager.addTask(SerializeHelper.readWorldCraftingTask((CompoundTag) task));
                }
            }

            return manager;
        }
    }
}
