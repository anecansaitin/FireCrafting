package cn.anecansaitin.firecrafting.common.crafting.wordtask;

import cn.anecansaitin.firecrafting.ModConfig;
import cn.anecansaitin.firecrafting.api.common.serializer.IWorldCraftingTaskSerializer;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingSerializers;
import com.ibm.icu.impl.Pair;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Rotation;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.ArrayList;
import java.util.List;

public class BlockWorldTask extends AbstractWorldCraftingTask {
    private final List<Pair<BlockPos, Block>> product;
    private int counter;
    private final int maxTime = ModConfig.TASK_DEAD_LINE.get();
    private int time = maxTime;

    public BlockWorldTask(BlockPos pos, ServerLevel world, List<Pair<BlockPos, Block>> product) {
        super(pos, world, 2);
        this.product = product;
        counter = product.size();
    }

    public BlockWorldTask(BlockPos pos, ServerLevel world, List<Pair<BlockPos, Block>> product, int orientation) {
        super(pos, world, orientation);
        this.product = rotation(product);
        counter = product.size();
    }

    public BlockWorldTask(BlockPos pos, ServerLevel world, List<Pair<BlockPos, Block>> product, int orientation, int counter, int time) {
        super(pos, world, orientation);
        this.product = rotation(product);
        this.counter = counter;
        this.time = time;
    }

    //将坐标按照orientation进行旋转
    private List<Pair<BlockPos, Block>> rotation(List<Pair<BlockPos, Block>> product) {
        //上下两个方向就不让旋转了，只能调整东南西北
        Rotation rotation = switch (getOrientation()) {
            case 3 ->
                //南
                    Rotation.CLOCKWISE_180;
            case 4 ->
                //西
                    Rotation.COUNTERCLOCKWISE_90;
            case 5 ->
                //东
                    Rotation.CLOCKWISE_90;
            default ->
                //其他
                    Rotation.NONE;
        };

        for (int i = 0, productSize = product.size(); i < productSize; i++) {
            Pair<BlockPos, Block> pair = product.get(i);
            product.set(i, Pair.of(pair.first.rotate(rotation), pair.second));
        }

        return product;
    }

    @Override
    public TickResult tick() {
        Level world = getWorld();
        Pair<BlockPos, Block> pair = product.get(product.size() - counter);
        BlockPos aimPos = pair.first.offset(getPos());

        if (checkPosIllegal(aimPos)) {
            return TickResult.UNCHANGED;
        }

        //坐标不是空气方块或火焰本身
        if (!world.isEmptyBlock(aimPos) && !aimPos.equals(getPos())) {
            time--;
            return TickResult.CHANGED;
        }

        counter--;
        world.setBlockAndUpdate(aimPos, pair.second.defaultBlockState());

        if (counter <= 0)
            setCompleted(true);

        return TickResult.DONE;
    }

    public List<Pair<BlockPos, Block>> getProduct() {
        return product;
    }

    public int getCounter() {
        return counter;
    }

    @Override
    public boolean isTimeout() {
        return time <= 0;
    }

    @Override
    public IWorldCraftingTaskSerializer<?> getSerializer() {
        return FireCraftingSerializers.BLOCK_WORLD_TASK.get();
    }

    public static class Serializer implements IWorldCraftingTaskSerializer<BlockWorldTask> {
        @Override
        public Tag toNBT(BlockWorldTask blockWorldTask) {
            CompoundTag root = new CompoundTag();
            ListTag blocks = new ListTag();
            root.put("blocks", blocks);

            //存方块
            for (Pair<BlockPos, Block> block : blockWorldTask.getProduct()) {
                CompoundTag blockNBT = new CompoundTag();
                BlockPos pos = block.first;
                blockNBT.putIntArray("pos", new int[]{pos.getX(), pos.getY(), pos.getZ()});
                blockNBT.putString("block", ForgeRegistries.BLOCKS.getKey(block.second).toString());
                blocks.add(blockNBT);
            }

            //存方向
            root.putInt("orientation", blockWorldTask.getOrientation());
            //存计数器
            root.putInt("counter", blockWorldTask.getCounter());
            //存世界
            writeWorld(root, blockWorldTask.getWorld());
            //存坐标
            writePos(root, blockWorldTask.getPos());
            //存时间
            root.putInt("time", blockWorldTask.time);
            return root;
        }

        @Override
        public BlockWorldTask fromNBT(Tag nbt) {
            if (nbt.getId() != Tag.TAG_COMPOUND)
                throw new RuntimeException("Illegal NBT. Require compound tag.");

            CompoundTag root = (CompoundTag) nbt;
            BlockPos pos;
            ServerLevel world;
            List<Pair<BlockPos, Block>> product = new ArrayList<>();
            int orientation;
            int counter;
            int time;

            //读方块
            for (Tag blockTag : root.getList("blocks", Tag.TAG_COMPOUND)) {
                CompoundTag blockNBT = (CompoundTag) blockTag;
                int[] posNBT = blockNBT.getIntArray("pos");
                String blockName = blockNBT.getString("block");
                product.add(Pair.of(new BlockPos(posNBT[0], posNBT[1], posNBT[2]), ForgeRegistries.BLOCKS.getValue(new ResourceLocation(blockName))));
            }

            //读方向
            orientation = root.getInt("orientation");
            //读计数器
            counter = root.getInt("counter");
            //读世界
            world = readWorld(root);
            //读坐标
            pos = readPos(root);
            //读时间
            time = root.getInt("time");
            return new BlockWorldTask(pos, world, product, orientation, counter, time);
        }
    }
}
