package cn.anecansaitin.firecrafting.common.crafting.recipe;

import cn.anecansaitin.firecrafting.api.common.crafting.IFireRecipe;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.util.RecipeMatcher;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public abstract class AbstractFireRecipe implements IFireRecipe {
    protected final ResourceLocation id;
    //最多9种物品
    protected final NonNullList<Ingredient> ingredients;
    protected final NonNullList<Block> fires;
    protected final Component description;

    public AbstractFireRecipe(ResourceLocation id, NonNullList<Ingredient> ingredients, NonNullList<Block> fires) {
        this(id, ingredients, fires, Component.empty());
    }

    public AbstractFireRecipe(ResourceLocation id, NonNullList<Ingredient> ingredients, NonNullList<Block> fires, Component description) {
        this.id = id;
        this.ingredients = ingredients;
        this.fires = fires;
        this.description = description;
    }

    @Override
    public @NotNull NonNullList<Ingredient> getIngredients() {
        return ingredients;
    }

    @Override
    public NonNullList<Block> getFires() {
        return fires;
    }

    @Override
    public boolean matches(List<ItemStack> items, BlockPos pos, ServerLevel world) {
        //检查火焰
        BlockState block = world.getBlockState(pos);
        boolean checkFire = false;

        for (Block fire : fires) {
            if (block.is(fire)) {
                checkFire = true;
                break;
            }
        }

        if (!checkFire)
            return false;

        //检查原料
        int[] matches = RecipeMatcher.findMatches(items, getIngredients());
        return matches != null;
    }

    @Override
    public Component getDescription() {
        return description;
    }

    @Override
    public @NotNull ResourceLocation getId() {
        return id;
    }
}
