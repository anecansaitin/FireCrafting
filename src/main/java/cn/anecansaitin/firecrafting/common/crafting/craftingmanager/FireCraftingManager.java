package cn.anecansaitin.firecrafting.common.crafting.craftingmanager;

import cn.anecansaitin.firecrafting.api.common.crafting.ITimedItems;
import cn.anecansaitin.firecrafting.api.common.crafting.IFireCraftingManager;
import cn.anecansaitin.firecrafting.api.common.crafting.IFireRecipe;
import cn.anecansaitin.firecrafting.api.common.crafting.IWorldCraftingTask;
import cn.anecansaitin.firecrafting.api.common.serializer.IFireCraftingManagerSerializer;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingRecipeTypes;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingSerializers;
import cn.anecansaitin.firecrafting.common.serializer.SerializeHelper;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.ItemStack;

import java.util.*;
import java.util.function.Function;

//todo 更合适的任务队列数据结构 Array-Linked List ?
public class FireCraftingManager implements IFireCraftingManager {
    private final HashMap<BlockPos, ITimedItems> timedItems = new HashMap<>();
    //因为需要经常从中间删除或添加
    private final LinkedList<IWorldCraftingTask> tasks = new LinkedList<>();
    private final Function<BlockPos, ITimedItems> timedItemsFunction = p -> IFireCraftingManager.createTimedItems();

    @Override
    public IFireRecipe matchRecipe(List<ItemStack> items, BlockPos pos, ServerLevel world) {
        List<IFireRecipe> recipes = world.getRecipeManager().getAllRecipesFor(FireCraftingRecipeTypes.FIRE_CRAFTING.get());
        IFireRecipe result = null;

        //根据物品的数量添加物品
        ArrayList<ItemStack> list = new ArrayList<>();

        for (ItemStack item : items)
            for (int i = 0; i < item.getCount(); i++)
                list.add(item);

        for (IFireRecipe recipe : recipes)
            if (recipe.matches(list, pos, world))
                result = recipe;

        return result;
    }

    @Override
    public boolean hasTimedItems(BlockPos pos) {
        return getTimedItems().containsKey(pos);
    }

    @Override
    public void addItem(BlockPos pos, ItemStack item) {
        ITimedItems timedItems = getTimedItems(pos);
        timedItems.addItem(item);
    }

    @Override
    public void addItems(BlockPos pos, List<ItemStack> items) {
        ITimedItems timedItems = getTimedItems(pos);
        timedItems.addItems(items);
    }

    protected void putTimedItems(BlockPos pos, ITimedItems items) {
        getTimedItems().put(pos, items);
    }

    @Override
    public ITimedItems getTimedItems(BlockPos pos) {
        return getTimedItems().computeIfAbsent(pos, timedItemsFunction);
    }

    @Override
    public Map<BlockPos, ITimedItems> getTimedItems() {
        return timedItems;
    }

    @Override
    public void removeTimedItems(BlockPos pos) {
        getTimedItems().remove(pos);
    }

    @Override
    public IWorldCraftingTask.TickResult performTask(IWorldCraftingTask task) {
        return task.tick();
    }

    @Override
    public void addTask(IFireRecipe recipe, List<ItemStack> items, BlockPos pos, ServerLevel world) {
        addTask(recipe.getWorldTask(items, pos, world));
    }

    protected void addTask(IWorldCraftingTask task) {
        getTasks().add(task);
    }

    @Override
    public List<IWorldCraftingTask> getTasks() {
        return tasks;
    }

    @Override
    public void removeTask(int index) {
        getTasks().remove(index);
    }

    @Override
    public IFireCraftingManagerSerializer<?> getSerialize() {
        return FireCraftingSerializers.FIRE_CRAFTING_MANAGER.get();
    }

    public static final class Serializer implements IFireCraftingManagerSerializer<FireCraftingManager> {
        @Override
        public Tag toNBT(FireCraftingManager manager) {
            CompoundTag root = new CompoundTag();
            Map<BlockPos, ITimedItems> items = manager.getTimedItems();
            //存物品
            if (items.size() > 0) {
                ListTag itemsNBT = new ListTag();
                root.put("items", itemsNBT);

                for (Map.Entry<BlockPos, ITimedItems> entry : items.entrySet()) {
                    CompoundTag node = new CompoundTag();
                    itemsNBT.add(node);
                    //存坐标
                    BlockPos pos = entry.getKey();
                    node.putIntArray("pos", new int[]{pos.getX(), pos.getY(), pos.getZ()});
                    //存物品
                    ITimedItems timedItems = entry.getValue();
                    node.put("items", SerializeHelper.timedItemsNBT(timedItems));
                }
            }

            //存任务
            List<IWorldCraftingTask> tasks = manager.getTasks();

            if (tasks.size() > 0) {
                ListTag tasksNBT = new ListTag();
                root.put("tasks", tasksNBT);

                for (IWorldCraftingTask task : tasks)
                    tasksNBT.add(SerializeHelper.worldCraftingTaskNBT(task));
            }

            return root;
        }

        @Override
        public FireCraftingManager fromNBT(Tag nbt) {
            if (nbt.getId() != Tag.TAG_COMPOUND)
                throw new RuntimeException("Illegal NBT. Require compound tag.");

            FireCraftingManager manager = new FireCraftingManager();
            CompoundTag compoundNBT = (CompoundTag) nbt;
            //取物品
            if (compoundNBT.contains("items", Tag.TAG_LIST)) {
                for (Tag item : compoundNBT.getList("items", Tag.TAG_COMPOUND)) {
                    int[] pos = ((CompoundTag) item).getIntArray("pos");
                    ITimedItems timedItems = SerializeHelper.readTimedItems(((CompoundTag) item).getCompound("items"));
                    manager.putTimedItems(new BlockPos(pos[0], pos[1], pos[2]), timedItems);
                }
            }
            //取任务
            if (compoundNBT.contains("tasks", Tag.TAG_LIST)) {
                for (Tag task : compoundNBT.getList("tasks", Tag.TAG_COMPOUND)) {
                    manager.addTask(SerializeHelper.readWorldCraftingTask((CompoundTag) task));
                }
            }

            return manager;
        }
    }
}
