package cn.anecansaitin.firecrafting.common.crafting.timeditems;

import cn.anecansaitin.firecrafting.api.common.serializer.ITimedItemsSerializer;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingSerializers;
import cn.anecansaitin.firecrafting.common.item.Catalyst;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.world.item.ItemStack;

import java.util.List;

/**
 * 只有当物品为催化剂时才会设为最新，并触发配方检测
 */
public class CatalystTimedItems extends TimedItems {
    public CatalystTimedItems() {
        setExpired();
    }

    @Override
    public void addItem(ItemStack item) {
        silentAddItem(item);
        setTime();

        if (item.getItem() instanceof Catalyst)
            setLatest();
    }

    @Override
    public void addItems(List<ItemStack> items) {
        silentAddItems(items);
        setTime();

        if (items.get(items.size() - 1).getItem() instanceof Catalyst)
            setLatest();
    }

    @Override
    public ITimedItemsSerializer<?> getSerializer() {
        return FireCraftingSerializers.CATALYST_TIMED_ITEMS.get();
    }

    public static final class Serializer implements ITimedItemsSerializer<CatalystTimedItems> {
        @Override
        public Tag toNBT(CatalystTimedItems timedItems) {
            CompoundTag root = new CompoundTag();
            //存物品
            ListTag items = new ListTag();
            root.put("items", items);

            for (ItemStack item : timedItems.getItems())
                items.add(item.serializeNBT());

            //存时间
            root.putInt("time", timedItems.getTime());
            //存更新
            root.putBoolean("latest", timedItems.isLatest());
            return root;
        }

        @Override
        public CatalystTimedItems fromNBT(Tag nbt) {
            if (nbt.getId() != Tag.TAG_COMPOUND)
                throw new RuntimeException("Illegal NBT. Require compound tag.");

            CatalystTimedItems timedItems = new CatalystTimedItems();
            CompoundTag compoundNBT = (CompoundTag) nbt;
            //取物品
            for (Tag itemNBT : compoundNBT.getList("items", Tag.TAG_COMPOUND)) {
                ItemStack item = ItemStack.of((CompoundTag) itemNBT);
                timedItems.silentAddItem(item);
            }
            //取时间
            timedItems.setTime(compoundNBT.getInt("time"));
            //取更新
            if (!compoundNBT.getBoolean("latest"))
                timedItems.setExpired();

            return timedItems;
        }
    }
}
