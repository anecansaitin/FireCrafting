package cn.anecansaitin.firecrafting.common.crafting.wordtask;

import cn.anecansaitin.firecrafting.api.common.serializer.IWorldCraftingTaskSerializer;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingSerializers;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.phys.AABB;

import java.util.List;

public class EffectWorldTask extends AbstractWorldCraftingTask{
    private final MobEffectInstance effect;
    private final int radius;

    public EffectWorldTask(BlockPos pos, ServerLevel world, MobEffectInstance effect, int radius) {
        super(pos, world, -1);
        this.effect = effect;
        this.radius = radius;
    }

    @Override
    public TickResult tick() {
        ServerLevel world = getWorld();
        BlockPos pos = getPos();
        List<LivingEntity> entities = world.getEntitiesOfClass(LivingEntity.class, new AABB(pos).inflate(radius));

        for (LivingEntity entity : entities) {
            //每个实体都要新实例化一个效果，不然大家共用一个会出错的
            entity.addEffect(new MobEffectInstance(effect));
        }

        setCompleted(true);
        return TickResult.DONE;
    }

    @Override
    public IWorldCraftingTaskSerializer<?> getSerializer() {
        return FireCraftingSerializers.EFFECT_WORLD_TASK.get();
    }

    public static class Serializer implements IWorldCraftingTaskSerializer<EffectWorldTask> {
        @Override
        public Tag toNBT(EffectWorldTask effectWorldTask) {
            CompoundTag root = new CompoundTag();
            //坐标
            writePos(root, effectWorldTask.getPos());
            //世界
            writeWorld(root, effectWorldTask.getWorld());
            //半径
            root.putInt("radius", effectWorldTask.radius);
            //效果
            root.put("effect", effectWorldTask.effect.save(new CompoundTag()));
            return root;
        }

        @Override
        public EffectWorldTask fromNBT(Tag nbt) {
            if (!(nbt instanceof CompoundTag root)) {
                throw new RuntimeException("Illegal NBT. Require compound tag.");
            }

            BlockPos pos = readPos(root);
            ServerLevel world = readWorld(root);
            int radius = root.getInt("radius");
            MobEffectInstance effect = MobEffectInstance.load(root.getCompound("effect"));
            return new EffectWorldTask(pos, world, effect, radius);
        }
    }
}
