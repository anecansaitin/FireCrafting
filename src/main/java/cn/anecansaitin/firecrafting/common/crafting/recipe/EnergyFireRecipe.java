package cn.anecansaitin.firecrafting.common.crafting.recipe;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.crafting.IMachineCraftingTask;
import cn.anecansaitin.firecrafting.api.common.crafting.IWorldCraftingTask;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingRecipeTypes;
import cn.anecansaitin.firecrafting.common.crafting.wordtask.EnergyWorldTask;
import cn.anecansaitin.firecrafting.common.item.Catalyst;
import com.google.gson.JsonObject;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.level.block.Block;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class EnergyFireRecipe extends AbstractFireRecipe {
    private final int product;
    private final int tick;

    public EnergyFireRecipe(ResourceLocation id, NonNullList<Ingredient> ingredients, NonNullList<Block> fires, int product, int tick) {
        super(id, ingredients, fires);
        this.product = product;
        this.tick = tick;
    }

    @Override
    public IWorldCraftingTask getWorldTask(List<ItemStack> items, BlockPos pos, ServerLevel world) {
        return new EnergyWorldTask(pos, world, product, tick);
    }

    @Override
    public IWorldCraftingTask getWorldTask(List<ItemStack> items, BlockPos pos, ServerLevel world, int catalystIndex) {
        return new EnergyWorldTask(pos, world, Catalyst.getOrientation(items.get(catalystIndex)), product, tick);
    }

    @Override
    public IMachineCraftingTask getMachineTask() {
        return null;
    }

    public int getProduct() {
        return product;
    }

    public int getTick() {
        return tick;
    }

    @Override
    public @NotNull RecipeSerializer<?> getSerializer() {
        return FireCraftingRecipeTypes.ENERGY_FIRE_SERIALIZER.get();
    }

    public static class Serializer extends AbstractRecipeSerializer<EnergyFireRecipe> {
        public static final String NAME = FireCrafting.MOD_ID + ":energy_fire_crafting";

        @Override
        public @NotNull EnergyFireRecipe fromJson(@NotNull ResourceLocation id, @NotNull JsonObject jsonObject) {
            //原料
            NonNullList<Ingredient> ingredients = readIngredients(id, jsonObject);
            //火焰
            NonNullList<Block> fires = readFire(id, jsonObject);
            //能量
            JsonObject product = GsonHelper.getAsJsonObject(jsonObject, "product");
            //持续时间
            int ticks = GsonHelper.getAsInt(product, "ticks");
            //每tick能量
            int energy = GsonHelper.getAsInt(product, "energy");
            return new EnergyFireRecipe(id, ingredients, fires, energy, ticks);
        }

        @Nullable
        @Override
        public EnergyFireRecipe fromNetwork(@NotNull ResourceLocation id, @NotNull FriendlyByteBuf buf) {
            //原料
            NonNullList<Ingredient> ingredients = readIngredients(buf);
            //火焰
            NonNullList<Block> fire = readFire(buf);
            //产物
            //持续时间
            int ticks = buf.readVarInt();
            //能量
            int energy = buf.readVarInt();
            return new EnergyFireRecipe(id, ingredients, fire, energy, ticks);
        }

        @Override
        public void toNetwork(@NotNull FriendlyByteBuf buf, @NotNull EnergyFireRecipe recipe) {
            //原料
            writeIngredients(buf, recipe);
            //火焰
            writeFire(buf, recipe);
            //产物
            //持续时间
            buf.writeVarInt(recipe.tick);
            //能量
            buf.writeVarInt(recipe.product);
        }
    }
}
