package cn.anecansaitin.firecrafting.common.crafting.recipe;

import cn.anecansaitin.firecrafting.api.common.crafting.IFireRecipe;
import cn.anecansaitin.firecrafting.common.util.json.ModJsonReader;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import net.minecraft.core.NonNullList;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.List;

public abstract class AbstractRecipeSerializer<T extends IFireRecipe> implements RecipeSerializer<T>{
    protected NonNullList<Ingredient> readIngredients(ResourceLocation id, JsonObject jsonObject) {
        //原料数量
        int[] ingredientCount = null;

        if (GsonHelper.isArrayNode(jsonObject, "ingredient_count")) {
            JsonArray count = GsonHelper.getAsJsonArray(jsonObject, "ingredient_count");
            ingredientCount = new int[count.size()];
            int i = 0;

            for (JsonElement element : count)
                ingredientCount[i++] = element.getAsInt();
        }

        //原料
        NonNullList<Ingredient> ingredients = ModJsonReader.ingredientsFromJson(jsonObject, "ingredients");

        if (ingredients.isEmpty())
            throw new JsonParseException("No ingredients for fire recipe. Recipe id: " + id);
        else if (ingredients.size() > 9)
            throw new JsonParseException("Too many ingredients item fire recipe. The maximum is 9. Recipe id: " + id);

        //整合原料与数量
        if (ingredientCount != null) {
            NonNullList<Ingredient> list = NonNullList.create();

            for (int i = 0, ingredientsSize = ingredients.size(); i < ingredientsSize; i++) {
                Ingredient ingredient = ingredients.get(i);
                int count = ingredientCount[i];

                for (int j = 0; j < count; j++)
                    list.add(ingredient);
            }

            ingredients = list;
        }

        return ingredients;
    }

    protected NonNullList<Ingredient> readIngredients(FriendlyByteBuf buf) {
        NonNullList<Ingredient> ingredients = NonNullList.withSize(buf.readVarInt(), Ingredient.EMPTY);
        ingredients.replaceAll(ignored -> Ingredient.fromNetwork(buf));
        return ingredients;
    }

    protected void writeIngredients(FriendlyByteBuf buf, T recipe) {
        NonNullList<Ingredient> ingredients = recipe.getIngredients();
        buf.writeVarInt(ingredients.size());

        for (Ingredient ingredient : ingredients)
            ingredient.toNetwork(buf);
    }

    protected NonNullList<Block> readFire(ResourceLocation id, JsonObject jsonObject) {
        List<Block> fire1 = ModJsonReader.blocksFromJson(jsonObject, "fire");

        if (fire1.isEmpty())
            throw new JsonParseException("No fire block for fire recipe. Recipe id: " + id);

        NonNullList<Block> fire = NonNullList.withSize(fire1.size(), Blocks.AIR);

        for (int i = 0, fireSize = fire.size(); i < fireSize; i++)
            fire.set(i, fire1.get(i));

        return fire;
    }

    protected NonNullList<Block> readFire(FriendlyByteBuf buf) {
        NonNullList<Block> fire = NonNullList.withSize(buf.readVarInt(), Blocks.AIR);
        fire.replaceAll(ignored -> ForgeRegistries.BLOCKS.getValue(buf.readResourceLocation()));
        return fire;
    }

    protected void writeFire(FriendlyByteBuf buf, T recipe) {
        buf.writeVarInt(recipe.getFires().size());

        for (Block fire : recipe.getFires())
            buf.writeResourceLocation(ForgeRegistries.BLOCKS.getKey(fire));
    }
}
