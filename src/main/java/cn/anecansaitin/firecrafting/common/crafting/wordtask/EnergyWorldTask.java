package cn.anecansaitin.firecrafting.common.crafting.wordtask;

import cn.anecansaitin.firecrafting.api.common.damagesource.ModDamageSources;
import cn.anecansaitin.firecrafting.api.common.serializer.IWorldCraftingTaskSerializer;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingSerializers;
import cn.anecansaitin.firecrafting.common.util.Directions;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntitySelector;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.IEnergyStorage;

import java.util.List;

public class EnergyWorldTask extends AbstractWorldCraftingTask {
    private final int product;
    private int tick;

    public EnergyWorldTask(BlockPos pos, ServerLevel world, int orientation, int product, int tick) {
        super(pos, world, orientation);
        this.product = product;
        this.tick = tick;
    }

    public EnergyWorldTask(BlockPos pos, ServerLevel world, int product, int tick) {
        this(pos, world, -1, product, tick);
    }

    @Override
    public TickResult tick() {
        //尝试发电
        if (getOrientation() >= 0 && getOrientation() <= 5) {
            //有指定方向
            Direction direction = Direction.from3DDataValue(getOrientation());
            BlockPos aimPos = getPos().relative(direction);

            if (checkPosIllegal(aimPos)) {
                return TickResult.UNCHANGED;
            }

            int residue = storeInContainer(aimPos, direction.getOpposite(), product);

            if (residue > 0)
                //没得存就漏电
                leakage(residue, getWorld());
        } else {
            int residue = product;

            for (Direction direction : Directions.DIRECTIONS) {
                BlockPos aimPos = getPos().relative(direction);

                if (checkPosIllegal(aimPos)) {
                    continue;
                }

                residue = storeInContainer(aimPos, direction.getOpposite(), residue);

                if (residue <= 0)
                    break;
            }

            if (residue > 0)
                leakage(residue, getWorld());
        }

        tick--;

        if (tick <= 0)
            setCompleted(true);

        return TickResult.DONE;
    }

    //向指定方向的方块存入能量，返回未能存入的能量
    protected int storeInContainer(BlockPos pos, Direction direction, int product) {
        BlockEntity blockEntity = getWorld().getBlockEntity(pos);

        if (blockEntity == null)
            return product;

        LazyOptional<IEnergyStorage> lazyCap = blockEntity.getCapability(ForgeCapabilities.ENERGY, direction);

        if (!lazyCap.isPresent())
            return product;

        IEnergyStorage cap = lazyCap.orElseThrow(RuntimeException::new);
        return product - cap.receiveEnergy(product, false);
    }

    protected void leakage(int product, ServerLevel world) {
        List<Entity> entities = getWorld().getEntities((Entity) null, new AABB(getPos().offset(-5, -5, -5), getPos().offset(5, 5, 5)), EntitySelector.LIVING_ENTITY_STILL_ALIVE);

        for (Entity entity : entities) {
            entity.hurt(ModDamageSources.electricShock(world), Math.max(product / 400f, 0.5f));
        }
    }

    @Override
    public IWorldCraftingTaskSerializer<?> getSerializer() {
        return FireCraftingSerializers.ENERGY_WORLD_TASK.get();
    }

    public static class Serializer implements IWorldCraftingTaskSerializer<EnergyWorldTask> {
        @Override
        public Tag toNBT(EnergyWorldTask energyWorldTask) {
            CompoundTag root = new CompoundTag();
            //存产量
            root.putInt("product", energyWorldTask.product);
            //存时间
            root.putInt("tick", energyWorldTask.tick);
            //存方向
            root.putInt("orientation", energyWorldTask.getOrientation());
            //存世界
            writeWorld(root, energyWorldTask.getWorld());
            //存坐标
            writePos(root, energyWorldTask.getPos());
            return root;
        }

        @Override
        public EnergyWorldTask fromNBT(Tag nbt) {
            if (nbt.getId() != Tag.TAG_COMPOUND)
                throw new RuntimeException("Illegal NBT. Require compound tag.");

            CompoundTag root = (CompoundTag) nbt;
            BlockPos pos;
            ServerLevel world;
            int orientation;
            int tick;
            int product;

            pos = readPos(root);
            world = readWorld(root);
            orientation = root.getInt("orientation");
            tick = root.getInt("tick");
            product = root.getInt("product");
            return new EnergyWorldTask(pos, world, orientation, product, tick);
        }
    }
}
