package cn.anecansaitin.firecrafting.common.crafting.timeditems;

import cn.anecansaitin.firecrafting.ModConfig;
import cn.anecansaitin.firecrafting.api.common.crafting.ITimedItems;
import cn.anecansaitin.firecrafting.api.common.serializer.ITimedItemsSerializer;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingSerializers;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.world.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class TimedItems implements ITimedItems {
    private final ArrayList<ItemStack> items = new ArrayList<>();
    private final int startTime = ModConfig.RETENTION_TIME.get();
    private int currentTime = startTime;
    private boolean latest = true;

    @Override
    public void tick() {
        currentTime--;
    }

    @Override
    public boolean isOvertime() {
        return currentTime <= 0;
    }

    @Override
    public void setTime() {
        currentTime = startTime;
    }

    @Override
    public void setTime(int time) {
        currentTime = time;
    }

    @Override
    public int getTime() {
        return currentTime;
    }

    @Override
    public void addItem(ItemStack item) {
        silentAddItem(item);
        setTime();
        setLatest();
    }

    public void silentAddItem(ItemStack item) {
        getItems().add(item);
    }

    @Override
    public void addItems(List<ItemStack> items) {
        silentAddItems(items);
        setTime();
        setLatest();
    }

    public void silentAddItems(List<ItemStack> items) {
        getItems().addAll(items);
    }

    @Override
    public List<ItemStack> getItems() {
        return items;
    }

    @Override
    public boolean isLatest() {
        return latest;
    }

    @Override
    public void setLatest() {
        latest = true;
    }

    @Override
    public void setExpired() {
        latest = false;
    }

    @Override
    public ITimedItemsSerializer<?> getSerializer() {
        return FireCraftingSerializers.TIMED_ITEMS.get();
    }

    public static final class Serializer implements ITimedItemsSerializer<TimedItems> {
        @Override
        public Tag toNBT(TimedItems timedItems) {
            CompoundTag root = new CompoundTag();
            //存物品
            ListTag items = new ListTag();
            root.put("items", items);

            for (ItemStack item : timedItems.getItems())
                items.add(item.serializeNBT());

            //存时间
            root.putInt("time", timedItems.getTime());
            //存更新
            root.putBoolean("latest", timedItems.isLatest());
            return root;
        }

        @Override
        public TimedItems fromNBT(Tag nbt) {
            if (nbt.getId() != Tag.TAG_COMPOUND)
                throw new RuntimeException("Illegal NBT. Require compound tag.");

            TimedItems timedItems = new TimedItems();
            CompoundTag compoundNBT = (CompoundTag) nbt;
            //取物品
            for (Tag itemNBT : compoundNBT.getList("items", Tag.TAG_COMPOUND)) {
                ItemStack item = ItemStack.of((CompoundTag) itemNBT);
                timedItems.silentAddItem(item);
            }
            //取时间
            timedItems.setTime(compoundNBT.getInt("time"));
            //取更新
            if (!compoundNBT.getBoolean("latest"))
                timedItems.setExpired();

            return timedItems;
        }
    }
}
