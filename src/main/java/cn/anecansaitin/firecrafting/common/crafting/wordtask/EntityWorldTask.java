package cn.anecansaitin.firecrafting.common.crafting.wordtask;

import cn.anecansaitin.firecrafting.api.common.serializer.IWorldCraftingTaskSerializer;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingSerializers;
import com.ibm.icu.impl.Pair;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraftforge.registries.ForgeRegistries;

public class EntityWorldTask extends AbstractWorldCraftingTask {
    private final Pair<EntityType<?>, CompoundTag> result;

    public EntityWorldTask(BlockPos pos, ServerLevel world, Pair<EntityType<?>, CompoundTag> result) {
        super(pos, world, -1);
        this.result = result;
    }

    public EntityWorldTask(BlockPos pos, ServerLevel world, Pair<EntityType<?>, CompoundTag> result, int orientation) {
        super(pos, world, orientation);
        this.result = result;
    }

    @Override
    public TickResult tick() {
        BlockPos aimPos = switch (getOrientation()) {
            case 0 -> getPos().below();
            case 2 -> getPos().north();
            case 3 -> getPos().south();
            case 4 -> getPos().west();
            case 5 -> getPos().east();
            default -> getPos().above();
        };

        if (checkPosIllegal(aimPos)) {
            return TickResult.UNCHANGED;
        }

        //nbt在传递时要进行拷贝，避免多个实体用同一个对象
        Entity entity = result.first.spawn(getWorld(), result.second.copy(), null, aimPos, MobSpawnType.EVENT, true, false);

        if (entity != null) {
            entity.setRemainingFireTicks(-40);
        }

        setCompleted(true);
        return TickResult.DONE;
    }

    public Pair<EntityType<?>, CompoundTag> getResult() {
        return result;
    }

    @Override
    public IWorldCraftingTaskSerializer<?> getSerializer() {
        return FireCraftingSerializers.ENTITY_WORLD_TASK.get();
    }

    public static final class Serializer implements IWorldCraftingTaskSerializer<EntityWorldTask> {
        @Override
        public Tag toNBT(EntityWorldTask entityWorldTask) {
            CompoundTag root = new CompoundTag();
            //存实体
            Pair<EntityType<?>, CompoundTag> result = entityWorldTask.getResult();
            CompoundTag entity = new CompoundTag();
            root.put("entity", entity);
            entity.putString("id", ForgeRegistries.ENTITY_TYPES.getKey(result.first).toString());
            root.put("nbt", result.second);
            //存方向
            root.putInt("orientation", entityWorldTask.getOrientation());
            //存世界
            writeWorld(root, entityWorldTask.getWorld());
            //存坐标
            writePos(root, entityWorldTask.getPos());
            return root;
        }

        @Override
        public EntityWorldTask fromNBT(Tag nbt) {
            if (nbt.getId() != Tag.TAG_COMPOUND)
                throw new RuntimeException("Illegal NBT. Require compound tag.");

            CompoundTag root = (CompoundTag) nbt;
            Pair<EntityType<?>, CompoundTag> result;
            int orientation;
            BlockPos pos;
            ServerLevel world;
            //读实体
            CompoundTag entity = root.getCompound("entity");
            EntityType<?> entityType = ForgeRegistries.ENTITY_TYPES.getValue(new ResourceLocation(entity.getString("id")));
            CompoundTag entityNbt = entity.getCompound("nbt");
            result = Pair.of(entityType, entityNbt);
            //读朝向
            orientation = root.getInt("orientation");
            //读世界
            world = readWorld(root);
            //读坐标
            pos = readPos(root);
            return new EntityWorldTask(pos, world, result, orientation);
        }
    }
}
