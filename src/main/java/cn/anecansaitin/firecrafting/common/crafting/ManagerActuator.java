package cn.anecansaitin.firecrafting.common.crafting;

import cn.anecansaitin.firecrafting.ModConfig;
import cn.anecansaitin.firecrafting.api.common.crafting.IFireCraftingManager;
import cn.anecansaitin.firecrafting.api.common.crafting.ITimedItems;
import cn.anecansaitin.firecrafting.api.common.crafting.IWorldCraftingTask;
import cn.anecansaitin.firecrafting.api.common.crafting.IFireRecipe;
import cn.anecansaitin.firecrafting.common.event.FireCraftingHooks;
import cn.anecansaitin.firecrafting.common.serializer.SerializeHelper;
import cn.anecansaitin.firecrafting.common.world.saveddata.FireCraftingSavedData;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public record ManagerActuator(IFireCraftingManager manager, FireCraftingSavedData data) {
    public void tick(ServerLevel world) {
        //保存用于移除的坐标
        ArrayList<BlockPos> remove = new ArrayList<>();
        Map<BlockPos, ITimedItems> itemsMap = manager.getTimedItems();

        for (Map.Entry<BlockPos, ITimedItems> entry : itemsMap.entrySet()) {
            BlockPos pos = entry.getKey();

            if (checkPosInvalid(world, pos)) {
                //不在合理范围内时，跳过
                continue;
            }

            ITimedItems items = entry.getValue();

            if (items.isLatest()) {
                //内容更新过了才进行匹配
                IFireRecipe recipe = manager.matchRecipe(items.getItems(), pos, world);
                //匹配过一次了就要设为过期
                items.setExpired();

                if (recipe != null) {
                    remove.add(pos);
                    //获取配方
                    manager.addTask(recipe, items.getItems(), pos, world);
                    continue;
                }
            }

            //推进计时器
            items.tick();
            setDirty();

            //超时检测
            if (items.isOvertime()) {
                remove.add(pos);
            }
        }

        //移除坐标
        for (BlockPos pos : remove) {
            manager.removeTimedItems(pos);
        }

        //任务执行
        List<IWorldCraftingTask> tasks = manager.getTasks();

        for (int i = tasks.size() - 1; i >= 0; i--) {
            IWorldCraftingTask task = tasks.get(i);
            FireCraftingHooks.worldCraftingTaskExecutionPre(task);
            IWorldCraftingTask.TickResult result = manager.performTask(task);

            switch (result) {
                case DONE, CHANGED -> setDirty();
            }

            FireCraftingHooks.worldCraftingTaskExecutionPost(task, result);

            //移除已完成与超时的任务
            if (task.isCompleted() || task.isTimeout()) {
                manager.removeTask(i);
                setDirty();
            }
        }
    }

    //检查坐标是否可用
    private boolean checkPosInvalid(ServerLevel world, BlockPos pos) {
        //未加载，跳过
        if (!world.isLoaded(pos))
            return true;

        //不是允许坐标的范围内
        return !world.isInWorldBounds(pos);
    }

    public void addItem(BlockPos pos, ItemStack item) {
        ITimedItems items = manager.getTimedItems(pos);

        //一个配方最多有9个物品原料，考虑到催化剂，因此最多10个
        if (items.getItems().size() > 10) {
            return;
        }

        manager.addItem(pos, item);
        setDirty();
    }

    public void addItems(BlockPos pos, List<ItemStack> items) {
        manager.addItems(pos, items);
        setDirty();
    }

    public ITimedItems getItems(BlockPos pos) {
        return manager.getTimedItems(pos);
    }

    public boolean hasItems(BlockPos pos) {
        return manager.hasTimedItems(pos);
    }

    private void setDirty() {
        data.setDirty();
    }

    public static ManagerActuator read(CompoundTag nbt) {
        if (!nbt.contains("manager", Tag.TAG_COMPOUND)) {
            throw new RuntimeException("Illegal NBT. Require compound tag.");
        }

        return create(nbt);
    }

    public CompoundTag save(CompoundTag nbt) {
        nbt.put("manager", SerializeHelper.fireCraftingManagerNBT(manager));
        return nbt;
    }

    //从nbt中读取manager并创建data
    private static ManagerActuator create(CompoundTag nbt) {
        ManagerActuator actuator = new ManagerActuator(SerializeHelper.readFireCraftingManager(nbt.getCompound("manager")), new FireCraftingSavedData());
        actuator.setDirty();
        return actuator;
    }

    //创建全新的manager和data
    public static ManagerActuator create() {
        return new ManagerActuator(createManager(), new FireCraftingSavedData());
    }

    private static IFireCraftingManager createManager() {
        return FireCraftingHooks.createFireCraftingManager(ModConfig.CATALYST.get());
    }
}
