package cn.anecansaitin.firecrafting.common.crafting.recipe;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.crafting.IMachineCraftingTask;
import cn.anecansaitin.firecrafting.api.common.crafting.IWorldCraftingTask;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingRecipeTypes;
import cn.anecansaitin.firecrafting.common.crafting.wordtask.ItemWorldTask;
import cn.anecansaitin.firecrafting.common.util.json.ModJsonReader;
import cn.anecansaitin.firecrafting.common.item.Catalyst;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.level.block.Block;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ItemFireRecipe extends AbstractFireRecipe {
    //只能容纳9种物品
    private final NonNullList<ItemStack> product;

    public ItemFireRecipe(ResourceLocation id, NonNullList<Ingredient> ingredients, NonNullList<Block> fires, NonNullList<ItemStack> product) {
        super(id, ingredients, fires);
        this.product = product;
    }

    @Override
    public IWorldCraftingTask getWorldTask(List<ItemStack> items, BlockPos pos, ServerLevel world) {
        return new ItemWorldTask(pos, world, getCopyProduct());
    }

    @Override
    public IWorldCraftingTask getWorldTask(List<ItemStack> items, BlockPos pos, ServerLevel world, int catalystIndex) {
        return new ItemWorldTask(pos, world, getCopyProduct(), Catalyst.getOrientation(items.get(catalystIndex)));
    }

    //返回产物的副本，确保这些产物不会影响的配方中记录的，避免对之后的配方产生影响
    protected List<ItemStack> getCopyProduct() {
        ArrayList<ItemStack> copy = new ArrayList<>(product.size());

        for (int i = 0; i < product.size(); i++) {
            copy.add(i, product.get(i).copy());
        }

        return copy;
    }

    @Override
    public IMachineCraftingTask getMachineTask() {
        //todo
        return null;
    }

    /**
     * 不要修改它的返回值
     * <br/>
     * 仅用于读取
     */
    public List<ItemStack> getProduct() {
        return product;
    }

    @Override
    public @NotNull RecipeSerializer<?> getSerializer() {
        return FireCraftingRecipeTypes.ITEM_FIRE_SERIALIZER.get();
    }

    public static class Serializer extends AbstractRecipeSerializer<ItemFireRecipe> {
        public static final String NAME = FireCrafting.MOD_ID + ":item_fire_crafting";

        @Override
        public @NotNull ItemFireRecipe fromJson(@NotNull ResourceLocation id, @NotNull JsonObject jsonObject) {
            //原料
            NonNullList<Ingredient> ingredients = readIngredients(id, jsonObject);
            //火焰
            NonNullList<Block> fire = readFire(id, jsonObject);

            //产物
            List<ItemStack> product1 = ModJsonReader.itemsFromJson(jsonObject, "product");

            if (product1.isEmpty())
                throw new JsonParseException("No product for item fire recipe. Recipe id: " + id);
            else if (product1.size() > 9)
                throw new JsonParseException("Product more then 9 for item fire recipe. Recipe id: " + id);

            NonNullList<ItemStack> product = NonNullList.withSize(product1.size(), ItemStack.EMPTY);

            for (int i = 0, productSize = product.size(); i < productSize; i++)
                product.set(i, product1.get(i));

            return new ItemFireRecipe(id, ingredients, fire, product);
        }

        @Nullable
        @Override
        public ItemFireRecipe fromNetwork(@NotNull ResourceLocation id, @NotNull FriendlyByteBuf buf) {
            //原料
            NonNullList<Ingredient> ingredients = readIngredients(buf);
            //火焰
            NonNullList<Block> fire = readFire(buf);
            //产物
            NonNullList<ItemStack> product = NonNullList.withSize(buf.readVarInt(), ItemStack.EMPTY);
            product.replaceAll(ignored -> buf.readItem());

            return new ItemFireRecipe(id, ingredients, fire, product);
        }

        @Override
        public void toNetwork(@NotNull FriendlyByteBuf buf, @NotNull ItemFireRecipe recipe) {
            //原料
            writeIngredients(buf, recipe);
            //火焰
            writeFire(buf, recipe);
            //产物
            buf.writeVarInt(recipe.product.size());

            for (ItemStack item : recipe.product)
                buf.writeItemStack(item, false);
        }
    }
}