package cn.anecansaitin.firecrafting.common.crafting.recipe;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.crafting.IMachineCraftingTask;
import cn.anecansaitin.firecrafting.api.common.crafting.IWorldCraftingTask;
import cn.anecansaitin.firecrafting.common.crafting.wordtask.EffectWorldTask;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingRecipeTypes;
import cn.anecansaitin.firecrafting.common.util.json.ModJsonReader;
import com.google.gson.JsonObject;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.level.block.Block;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class EffectFireRecipe extends AbstractFireRecipe {
    private final MobEffectInstance effect;
    private final int radius;

    public EffectFireRecipe(ResourceLocation id, NonNullList<Ingredient> ingredients, NonNullList<Block> fires, MobEffectInstance effect, int radius) {
        super(id, ingredients, fires);
        this.effect = effect;
        this.radius = radius;
    }

    @Override
    public IWorldCraftingTask getWorldTask(List<ItemStack> items, BlockPos pos, ServerLevel world) {
        return new EffectWorldTask(pos, world, new MobEffectInstance(effect), radius);
    }

    @Override
    public IWorldCraftingTask getWorldTask(List<ItemStack> items, BlockPos pos, ServerLevel world, int catalystIndex) {
        return getWorldTask(items, pos, world);
    }

    @Override
    public IMachineCraftingTask getMachineTask() {
        return null;
    }

    public MobEffectInstance getEffect() {
        return effect;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public @NotNull RecipeSerializer<?> getSerializer() {
        return FireCraftingRecipeTypes.EFFECT_FIRE_SERIALIZER.get();
    }

    public static class Serializer extends AbstractRecipeSerializer<EffectFireRecipe> {
        public static final String NAME = FireCrafting.MOD_ID + ":effect_fire_crafting";

        @Override
        public @NotNull EffectFireRecipe fromJson(@NotNull ResourceLocation id, @NotNull JsonObject json) {
            NonNullList<Ingredient> ingredients = readIngredients(id, json);
            NonNullList<Block> fire = readFire(id, json);
            MobEffectInstance effect = ModJsonReader.effectFromJson(json, "product");
            int radius = GsonHelper.getAsInt(GsonHelper.getAsJsonObject(json, "product"), "radius");
            return new EffectFireRecipe(id, ingredients, fire, effect, radius);
        }

        @Nullable
        @Override
        public EffectFireRecipe fromNetwork(@NotNull ResourceLocation id, @NotNull FriendlyByteBuf buf) {
            //读原料
            NonNullList<Ingredient> ingredients = readIngredients(buf);
            //读火焰
            NonNullList<Block> fire = readFire(buf);
            //读效果
            MobEffectInstance effect = MobEffectInstance.load(buf.readNbt());
            //读半径
            int radius = buf.readVarInt();
            return new EffectFireRecipe(id, ingredients, fire, effect, radius);
        }

        @Override
        public void toNetwork(@NotNull FriendlyByteBuf buf, @NotNull EffectFireRecipe recipe) {
            //写原料
            writeIngredients(buf, recipe);
            //写火焰
            writeFire(buf, recipe);
            //写效果
            buf.writeNbt(recipe.effect.save(new CompoundTag()));
            //写半径
            buf.writeVarInt(recipe.radius);
        }
    }
}
