package cn.anecansaitin.firecrafting.common.crafting.recipe;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.crafting.IMachineCraftingTask;
import cn.anecansaitin.firecrafting.api.common.crafting.IWorldCraftingTask;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingRecipeTypes;
import cn.anecansaitin.firecrafting.common.crafting.wordtask.EntityWorldTask;
import cn.anecansaitin.firecrafting.common.item.Catalyst;
import cn.anecansaitin.firecrafting.common.util.json.ModJsonReader;
import com.google.gson.JsonObject;
import com.ibm.icu.impl.Pair;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.ForgeRegistries;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class EntityFireRecipe extends AbstractFireRecipe {
    private final Pair<EntityType<?>, CompoundTag> product;

    public EntityFireRecipe(ResourceLocation id, NonNullList<Ingredient> ingredients, NonNullList<Block> fires, Pair<EntityType<?>, CompoundTag> product) {
        super(id, ingredients, fires);
        this.product = product;
    }

    @Override
    public IWorldCraftingTask getWorldTask(List<ItemStack> items, BlockPos pos, ServerLevel world) {
        return new EntityWorldTask(pos, world, product);
    }

    @Override
    public IWorldCraftingTask getWorldTask(List<ItemStack> items, BlockPos pos, ServerLevel world, int catalystIndex) {
        return new EntityWorldTask(pos, world, product, Catalyst.getOrientation(items.get(catalystIndex)));
    }

    @Override
    public IMachineCraftingTask getMachineTask() {
        return null;
    }

    //仅用于读取，不能修改
    public Pair<EntityType<?>, CompoundTag> getProduct() {
        return product;
    }

    @Override
    public @NotNull RecipeSerializer<?> getSerializer() {
        return FireCraftingRecipeTypes.ENTITY_FIRE_SERIALIZER.get();
    }

    public static class Serializer extends AbstractRecipeSerializer<EntityFireRecipe> {
        public static final String NAME = FireCrafting.MOD_ID + ":entity_fire_crafting";

        @Override
        public @NotNull EntityFireRecipe fromJson(@NotNull ResourceLocation id, @NotNull JsonObject jsonObject) {
            //原料
            NonNullList<Ingredient> ingredients = readIngredients(id, jsonObject);
            //火焰
            NonNullList<Block> fire = readFire(id, jsonObject);
            //产物
            Pair<EntityType<?>, CompoundTag> product = ModJsonReader.entityAndNbtFromJson(jsonObject, "product");
            return new EntityFireRecipe(id, ingredients, fire, product);
        }

        @Nullable
        @Override
        public EntityFireRecipe fromNetwork(@NotNull ResourceLocation id, @NotNull FriendlyByteBuf buf) {
            //原料
            NonNullList<Ingredient> ingredients = readIngredients(buf);
            //火焰
            NonNullList<Block> fire = readFire(buf);
            //产物
            EntityType<?> entity = ForgeRegistries.ENTITY_TYPES.getValue(buf.readResourceLocation());
            CompoundTag nbt = buf.readNbt();
            return new EntityFireRecipe(id, ingredients, fire, Pair.of(entity, nbt));
        }

        @Override
        public void toNetwork(@NotNull FriendlyByteBuf buf, @NotNull EntityFireRecipe recipe) {
            //原料
            writeIngredients(buf, recipe);
            //火焰
            writeFire(buf, recipe);
            //产物

            buf.writeResourceLocation(ForgeRegistries.ENTITY_TYPES.getKey(recipe.product.first));
            buf.writeNbt(recipe.product.second);
        }
    }
}
