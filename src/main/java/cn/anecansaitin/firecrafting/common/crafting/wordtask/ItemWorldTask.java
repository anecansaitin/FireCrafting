package cn.anecansaitin.firecrafting.common.crafting.wordtask;

import cn.anecansaitin.firecrafting.api.common.serializer.IWorldCraftingTaskSerializer;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingSerializers;
import cn.anecansaitin.firecrafting.common.util.Directions;
import cn.anecansaitin.firecrafting.common.util.DropHelper;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ItemWorldTask extends AbstractWorldCraftingTask {
    private final List<ItemStack> results;
    private static final Consumer<ItemEntity> customEntity = (itemEntity) -> {
        itemEntity.setNoGravity(true);
        itemEntity.setRemainingFireTicks(-6000);
        itemEntity.setDeltaMovement(0, 0, 0);
    };

    public ItemWorldTask(BlockPos pos, ServerLevel world, List<ItemStack> results) {
        super(pos, world, -1);
        this.results = results;
    }

    public ItemWorldTask(BlockPos pos, ServerLevel world, List<ItemStack> results, int orientation) {
        super(pos, world, orientation);
        this.results = results;
    }

    @Override
    public TickResult tick() {
        //尝试把物品放入容器
        if (getOrientation() >= 0 && getOrientation() <= 5) {
            //有指定方向
            Direction direction = Direction.from3DDataValue(getOrientation());

            if (checkPosIllegal(getPos().relative(direction))) {
                return TickResult.UNCHANGED;
            }

            //只向指定方向的方块存入物品
            storeInContainer(direction);
            //向指定方向生成掉落物
            dropToWorld(getPos().relative(direction));
            setCompleted(true);
        } else {
            //无指定方向
            for (Direction direction : Directions.DIRECTIONS) {
                //尝试向每个方向的方块存入物品
                if (getResults().isEmpty())
                    break;

                if (checkPosIllegal(getPos().relative(direction))) {
                    continue;
                }

                storeInContainer(direction);
            }

            if (checkPosIllegal(getPos().above())) {
                return TickResult.CHANGED;
            }

            //把物品生成为掉落物
            dropToWorld(getPos().above());
            setCompleted(true);
        }

        return TickResult.DONE;
    }

    //将产物生成为掉落物
    private void dropToWorld(BlockPos pos) {
        for (ItemStack item : getResults()) {
            if (item.isEmpty())
                continue;

            DropHelper.dropItemStack(getWorld(), pos, item, customEntity);
        }
    }

    //将产物存入指定方向的容器内
    private void storeInContainer(Direction direction) {
        BlockPos pos = getPos().relative(direction);
        BlockEntity blockEntity = getWorld().getBlockEntity(pos);

        if (blockEntity == null)
            return;

        LazyOptional<IItemHandler> lazyCap = blockEntity.getCapability(ForgeCapabilities.ITEM_HANDLER, direction.getOpposite());

        if (!lazyCap.isPresent())
            return;

        IItemHandler cap = lazyCap.orElseThrow(RuntimeException::new);

        for (int i = 0, resultsSize = getResults().size(); i < resultsSize; i++)
            getResults().set(i, ItemHandlerHelper.insertItem(cap, getResults().get(i), false));
    }

    public List<ItemStack> getResults() {
        return results;
    }

    @Override
    public IWorldCraftingTaskSerializer<?> getSerializer() {
        return FireCraftingSerializers.ITEM_WORLD_TASK.get();
    }

    public static final class Serializer implements IWorldCraftingTaskSerializer<ItemWorldTask> {

        @Override
        public Tag toNBT(ItemWorldTask itemWorldTask) {
            CompoundTag root = new CompoundTag();
            //存物品
            ListTag items = new ListTag();
            root.put("items", items);

            for (ItemStack item : itemWorldTask.getResults())
                items.add(item.serializeNBT());

            //存方向
            root.putInt("orientation", itemWorldTask.getOrientation());
            //存世界
            writeWorld(root, itemWorldTask.getWorld());
            //存坐标
            writePos(root, itemWorldTask.getPos());
            return root;
        }

        @Override
        public ItemWorldTask fromNBT(Tag nbt) {
            if (nbt.getId() != Tag.TAG_COMPOUND)
                throw new RuntimeException("Illegal NBT. Require compound tag.");

            CompoundTag root = (CompoundTag) nbt;
            BlockPos pos;
            ServerLevel world;
            List<ItemStack> results = new ArrayList<>();
            int orientation;

            //读物品
            for (Tag item : root.getList("items", Tag.TAG_COMPOUND))
                results.add(ItemStack.of((CompoundTag) item));

            //读方向
            orientation = root.getInt("orientation");
            //读世界
            world = readWorld(root);
            //读坐标
            pos = readPos(root);
            return new ItemWorldTask(pos, world, results, orientation);
        }
    }
}
