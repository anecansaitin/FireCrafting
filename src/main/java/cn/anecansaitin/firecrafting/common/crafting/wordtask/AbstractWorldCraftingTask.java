package cn.anecansaitin.firecrafting.common.crafting.wordtask;

import cn.anecansaitin.firecrafting.api.common.crafting.IWorldCraftingTask;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;

public abstract class AbstractWorldCraftingTask implements IWorldCraftingTask {
    private final BlockPos pos;
    private final ServerLevel world;
    private boolean completed = false;
    private final int orientation;

    public AbstractWorldCraftingTask(BlockPos pos, ServerLevel world, int orientation) {
        this.pos = pos;
        this.world = world;
        this.orientation = orientation;
    }

    @Override
    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    @Override
    public boolean isTimeout() {
        return false;
    }

    public BlockPos getPos() {
        return pos;
    }

    public ServerLevel getWorld() {
        return world;
    }

    public int getOrientation() {
        return orientation;
    }

    //检查坐标是否可用
    protected boolean checkPosIllegal(BlockPos pos) {
        //未加载，跳过
        if (!world.isLoaded(pos))
            return true;

        //不是允许坐标的范围内
        return !world.isInWorldBounds(pos);
    }
}
