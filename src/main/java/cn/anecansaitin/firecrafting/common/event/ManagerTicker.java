package cn.anecansaitin.firecrafting.common.event;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.common.crafting.ManagerActuator;
import cn.anecansaitin.firecrafting.common.world.saveddata.FireCraftingSavedData;
import net.minecraft.server.level.ServerLevel;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = FireCrafting.MOD_ID)
public class ManagerTicker {
    @SubscribeEvent
    public static void tick(TickEvent.LevelTickEvent event) {
        if (event.side.isClient() || (event.phase == TickEvent.Phase.END))
            return;

        ManagerActuator actuator = FireCraftingSavedData.getActuator((ServerLevel) event.level);
        actuator.tick((ServerLevel) event.level);
    }
}
