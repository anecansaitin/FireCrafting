package cn.anecansaitin.firecrafting.common.event;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.event.ItemBurnedInFireEvent;
import cn.anecansaitin.firecrafting.common.crafting.ManagerActuator;
import cn.anecansaitin.firecrafting.common.world.saveddata.FireCraftingSavedData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = FireCrafting.MOD_ID)
public class ItemBurned {
    /**
     * 物品被燃烧后放入合成系统内
     */
    @SubscribeEvent(priority = EventPriority.LOWEST)
    public static void onItemBurned(ItemBurnedInFireEvent event) {
        ItemEntity entity = (ItemEntity) event.getEntity();
        ItemStack item = entity.getItem();

        if (item.isEmpty())
            return;

        ManagerActuator actuator = FireCraftingSavedData.getActuator((ServerLevel) entity.getLevel());
        actuator.addItem(event.getFirePos(), item);
    }
}
