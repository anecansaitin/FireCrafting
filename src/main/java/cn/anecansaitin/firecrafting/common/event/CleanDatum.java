package cn.anecansaitin.firecrafting.common.event;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.common.world.saveddata.FireCraftingSavedData;
import net.minecraftforge.event.server.ServerStoppedEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = FireCrafting.MOD_ID)
public class CleanDatum {
    @SubscribeEvent
    public static void cleanDatum(ServerStoppedEvent event) {
        FireCraftingSavedData.cleanDatum();
    }
}
