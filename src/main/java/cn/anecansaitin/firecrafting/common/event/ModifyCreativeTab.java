package cn.anecansaitin.firecrafting.common.event;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingItems;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraftforge.event.CreativeModeTabEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = FireCrafting.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModifyCreativeTab {
    @SubscribeEvent
    public static void addItemToTab(CreativeModeTabEvent.BuildContents event) {
        if (event.getTab() == CreativeModeTabs.TOOLS_AND_UTILITIES && event.hasPermissions()) {
            event.accept(FireCraftingItems.CATALYST);
        }
    }
}
