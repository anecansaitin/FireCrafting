package cn.anecansaitin.firecrafting.common.event;

import cn.anecansaitin.firecrafting.api.common.crafting.IFireCraftingManager;
import cn.anecansaitin.firecrafting.api.common.crafting.ITimedItems;
import cn.anecansaitin.firecrafting.api.common.crafting.IWorldCraftingTask;
import cn.anecansaitin.firecrafting.api.common.event.CreateManagerEvent;
import cn.anecansaitin.firecrafting.api.common.event.CreateTimedItemsEvent;
import cn.anecansaitin.firecrafting.api.common.event.ItemBurnedInFireEvent;
import cn.anecansaitin.firecrafting.api.common.event.WorldCraftingTaskExecutionEvent;
import cn.anecansaitin.firecrafting.common.crafting.craftingmanager.CatalystCraftingManager;
import cn.anecansaitin.firecrafting.common.crafting.craftingmanager.FireCraftingManager;
import cn.anecansaitin.firecrafting.common.crafting.timeditems.CatalystTimedItems;
import cn.anecansaitin.firecrafting.common.crafting.timeditems.TimedItems;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.Entity;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.Event;

public class FireCraftingHooks {
    public static void OnItemBurnedInFire(Entity item, BlockPos firePos) {
        post(new ItemBurnedInFireEvent(item, firePos));
    }

    public static IFireCraftingManager createFireCraftingManager(boolean catalyst) {
        CreateManagerEvent event = new CreateManagerEvent(catalyst, catalyst ? new CatalystCraftingManager() : new FireCraftingManager());
        post(event);
        return event.getManager();
    }

    public static ITimedItems createTimedItems(boolean catalyst) {
        CreateTimedItemsEvent event = new CreateTimedItemsEvent(catalyst, catalyst ? new CatalystTimedItems() : new TimedItems());
        post(event);
        return event.getTimedItems();
    }

    public static <T extends IWorldCraftingTask> void worldCraftingTaskExecutionPre(T task) {
        post(new WorldCraftingTaskExecutionEvent.Pre<>(task));
    }

    public static <T extends IWorldCraftingTask> void worldCraftingTaskExecutionPost(T task, IWorldCraftingTask.TickResult result) {
        post(new WorldCraftingTaskExecutionEvent.Post<>(task, result));
    }

    private static void post(Event event) {
        MinecraftForge.EVENT_BUS.post(event);
    }
}
