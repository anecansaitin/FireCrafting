package cn.anecansaitin.firecrafting.common.event;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.serializer.IFireCraftingManagerSerializer;
import cn.anecansaitin.firecrafting.api.common.serializer.ITimedItemsSerializer;
import cn.anecansaitin.firecrafting.api.common.serializer.IWorldCraftingTaskSerializer;
import cn.anecansaitin.firecrafting.api.registries.FireCraftingRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.NewRegistryEvent;
import net.minecraftforge.registries.RegistryBuilder;

@Mod.EventBusSubscriber(modid = FireCrafting.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class NewRegistry {
    @SubscribeEvent
    public static void newRegistry(NewRegistryEvent event) {
        event.create(new RegistryBuilder<ITimedItemsSerializer<?>>()
                        .setName(new ResourceLocation(FireCrafting.MOD_ID, "timed_items_serializers"))
                        .allowModification(),
                r -> FireCraftingRegistries.TIMED_ITEMS_SERIALIZERS = r);

        event.create(new RegistryBuilder<IWorldCraftingTaskSerializer<?>>()
                        .setName(new ResourceLocation(FireCrafting.MOD_ID, "world_crafting_task_serializer"))
                        .allowModification(),
                r -> FireCraftingRegistries.WORLD_CRAFTING_TASK_SERIALIZER = r);

        event.create(new RegistryBuilder<IFireCraftingManagerSerializer<?>>()
                        .setName(new ResourceLocation(FireCrafting.MOD_ID, "fire_crafting_manager_serializer"))
                        .allowModification(),
                r -> FireCraftingRegistries.FIRE_CRAFTING_MANAGER_SERIALIZER = r);
    }
}
