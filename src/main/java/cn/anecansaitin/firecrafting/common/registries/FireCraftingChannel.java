package cn.anecansaitin.firecrafting.common.registries;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.integration.jade.ReceiveFireInfoPacket;
import cn.anecansaitin.firecrafting.integration.jade.RequestFireInfoPacket;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkRegistry;
import net.minecraftforge.network.simple.SimpleChannel;

import java.util.Optional;

public class FireCraftingChannel {
    private static final String version = "1.0";
    public static final SimpleChannel CHANNEL = NetworkRegistry.ChannelBuilder.named(new ResourceLocation(FireCrafting.MOD_ID, "networking")).clientAcceptedVersions(s -> s.equals(version)).serverAcceptedVersions(s -> s.equals(version)).networkProtocolVersion(() -> version).simpleChannel();

    public static void registry() {
        if (ModList.get().isLoaded("jade")) {
            CHANNEL.registerMessage(0, RequestFireInfoPacket.class, RequestFireInfoPacket::write, RequestFireInfoPacket::read, RequestFireInfoPacket::handler, Optional.of(NetworkDirection.PLAY_TO_SERVER));
            CHANNEL.registerMessage(1, ReceiveFireInfoPacket.class, ReceiveFireInfoPacket::write, ReceiveFireInfoPacket::read, ReceiveFireInfoPacket::handler, Optional.of(NetworkDirection.PLAY_TO_CLIENT));
        }
    }
}
