package cn.anecansaitin.firecrafting.common.registries;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.serializer.IFireCraftingManagerSerializer;
import cn.anecansaitin.firecrafting.api.common.serializer.ITimedItemsSerializer;
import cn.anecansaitin.firecrafting.api.common.serializer.IWorldCraftingTaskSerializer;
import cn.anecansaitin.firecrafting.api.registries.FireCraftingRegistries;
import cn.anecansaitin.firecrafting.common.crafting.craftingmanager.CatalystCraftingManager;
import cn.anecansaitin.firecrafting.common.crafting.craftingmanager.FireCraftingManager;
import cn.anecansaitin.firecrafting.common.crafting.timeditems.CatalystTimedItems;
import cn.anecansaitin.firecrafting.common.crafting.timeditems.TimedItems;
import cn.anecansaitin.firecrafting.common.crafting.wordtask.*;
import cn.anecansaitin.firecrafting.common.util.LoggerMarkers;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;

public class FireCraftingSerializers {
    public static final DeferredRegister<ITimedItemsSerializer<?>> TI_SERIALIZER = FireCraftingRegistries.createTimedItemsRegister(FireCrafting.MOD_ID);
    public static final DeferredRegister<IWorldCraftingTaskSerializer<?>> WCT_SERIALIZER = FireCraftingRegistries.createWorldCraftingTaskRegister(FireCrafting.MOD_ID);
    public static final DeferredRegister<IFireCraftingManagerSerializer<?>> FCM_SERIALIZER = FireCraftingRegistries.createFireCraftingManagerRegister(FireCrafting.MOD_ID);

    //计时物品⌚
    public static final RegistryObject<ITimedItemsSerializer<?>> TIMED_ITEMS = TI_SERIALIZER.register("timed_items", TimedItems.Serializer::new);
    public static final RegistryObject<ITimedItemsSerializer<?>> CATALYST_TIMED_ITEMS = TI_SERIALIZER.register("catalyst_timed_items", CatalystTimedItems.Serializer::new);
    //世界任务🌏
    public static final RegistryObject<IWorldCraftingTaskSerializer<?>> ITEM_WORLD_TASK = WCT_SERIALIZER.register("item_world_task", ItemWorldTask.Serializer::new);
    public static final RegistryObject<IWorldCraftingTaskSerializer<?>> BLOCK_WORLD_TASK = WCT_SERIALIZER.register("block_world_task", BlockWorldTask.Serializer::new);
    public static final RegistryObject<IWorldCraftingTaskSerializer<?>> ENTITY_WORLD_TASK = WCT_SERIALIZER.register("entity_world_task", EntityWorldTask.Serializer::new);
    public static final RegistryObject<IWorldCraftingTaskSerializer<?>> ENERGY_WORLD_TASK = WCT_SERIALIZER.register("energy_world_task", EnergyWorldTask.Serializer::new);
    public static final RegistryObject<IWorldCraftingTaskSerializer<?>> EFFECT_WORLD_TASK = WCT_SERIALIZER.register("effect_world_task", EffectWorldTask.Serializer::new);

    //烈焰配方处理器🖥️
    public static final RegistryObject<IFireCraftingManagerSerializer<FireCraftingManager>> FIRE_CRAFTING_MANAGER = FCM_SERIALIZER.register("fire_crafting_manager", FireCraftingManager.Serializer::new);
    public static final RegistryObject<IFireCraftingManagerSerializer<CatalystCraftingManager>> CATALYST_CRAFTING_MANAGER = FCM_SERIALIZER.register("catalyst_crafting_manager", CatalystCraftingManager.Serializer::new);

    public static void registry(IEventBus bus) {
        FireCrafting.getLogger().info(LoggerMarkers.MOD_INITIALIZATION, "Registry timed items serializer.");
        TI_SERIALIZER.register(bus);
        FireCrafting.getLogger().info(LoggerMarkers.MOD_INITIALIZATION, "Registry world crafting task serializer.");
        WCT_SERIALIZER.register(bus);
        FireCrafting.getLogger().info(LoggerMarkers.MOD_INITIALIZATION, "Registry fire crafting manager serializer.");
        FCM_SERIALIZER.register(bus);
    }
}
