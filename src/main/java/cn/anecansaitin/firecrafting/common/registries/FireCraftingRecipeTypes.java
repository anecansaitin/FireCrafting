package cn.anecansaitin.firecrafting.common.registries;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.crafting.IFireRecipe;
import cn.anecansaitin.firecrafting.common.crafting.recipe.*;
import cn.anecansaitin.firecrafting.common.util.LoggerMarkers;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class FireCraftingRecipeTypes {
    public static final DeferredRegister<RecipeType<?>> RECIPE = DeferredRegister.create(ForgeRegistries.RECIPE_TYPES, FireCrafting.MOD_ID);
    public static final DeferredRegister<RecipeSerializer<?>> SERIALIZER = DeferredRegister.create(ForgeRegistries.RECIPE_SERIALIZERS, FireCrafting.MOD_ID);
    public static final RegistryObject<RecipeType<IFireRecipe>> FIRE_CRAFTING = RECIPE.register(IFireRecipe.TYPE_ID, () -> RecipeType.simple(new ResourceLocation(FireCrafting.MOD_ID, "fire_crafting")));

    //序列化
    public static final RegistryObject<ItemFireRecipe.Serializer> ITEM_FIRE_SERIALIZER = SERIALIZER.register(ItemFireRecipe.Serializer.NAME.substring(FireCrafting.MOD_ID.length() + 1), ItemFireRecipe.Serializer::new);
    public static final RegistryObject<BlockFireRecipe.Serializer> BLOCK_FIRE_SERIALIZER = SERIALIZER.register(BlockFireRecipe.Serializer.NAME.substring(FireCrafting.MOD_ID.length() + 1), BlockFireRecipe.Serializer::new);
    public static final RegistryObject<EntityFireRecipe.Serializer> ENTITY_FIRE_SERIALIZER = SERIALIZER.register(EntityFireRecipe.Serializer.NAME.substring(FireCrafting.MOD_ID.length() + 1), EntityFireRecipe.Serializer::new);
    public static final RegistryObject<EnergyFireRecipe.Serializer> ENERGY_FIRE_SERIALIZER = SERIALIZER.register(EnergyFireRecipe.Serializer.NAME.substring(FireCrafting.MOD_ID.length() + 1), EnergyFireRecipe.Serializer::new);
    public static final RegistryObject<EffectFireRecipe.Serializer> EFFECT_FIRE_SERIALIZER = SERIALIZER.register(EffectFireRecipe.Serializer.NAME.substring(FireCrafting.MOD_ID.length() + 1), EffectFireRecipe.Serializer::new);

    public static void registry(IEventBus bus) {
        FireCrafting.getLogger().info(LoggerMarkers.MOD_INITIALIZATION, "Registry recipe.");
        RECIPE.register(bus);
        FireCrafting.getLogger().info(LoggerMarkers.MOD_INITIALIZATION, "Registry recipe serializer.");
        SERIALIZER.register(bus);
    }
}
