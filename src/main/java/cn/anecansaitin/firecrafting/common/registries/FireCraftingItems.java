package cn.anecansaitin.firecrafting.common.registries;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.common.item.Catalyst;
import cn.anecansaitin.firecrafting.common.util.LoggerMarkers;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Rarity;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class FireCraftingItems {
    public static final DeferredRegister<Item> ITEM = DeferredRegister.create(ForgeRegistries.ITEMS, FireCrafting.MOD_ID);

    public static final RegistryObject<Item> CATALYST = ITEM.register("catalyst", Catalyst::new);
    public static final RegistryObject<Item> ITEM_ICON = ITEM.register("item_icon", () -> new Item(new Item.Properties().rarity(Rarity.EPIC)));
    public static final RegistryObject<Item> ENERGY_ICON = ITEM.register("energy_icon", () -> new Item(new Item.Properties().rarity(Rarity.EPIC)));
    public static final RegistryObject<Item> ENTITY_ICON = ITEM.register("entity_icon", () -> new Item(new Item.Properties().rarity(Rarity.EPIC)));
    public static final RegistryObject<Item> BLOCK_ICON = ITEM.register("block_icon", () -> new Item(new Item.Properties().rarity(Rarity.EPIC)));
    public static final RegistryObject<Item> EFFECT_ICON = ITEM.register("effect_icon", () -> new Item(new Item.Properties().rarity(Rarity.EPIC)));

    public static void registry(IEventBus bus) {
        FireCrafting.getLogger().info(LoggerMarkers.MOD_INITIALIZATION, "Registry item.");
        ITEM.register(bus);
    }
}
