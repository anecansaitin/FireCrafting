package cn.anecansaitin.firecrafting.common.registries;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.common.util.LoggerMarkers;
import net.minecraft.world.level.block.AbstractGlassBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class FireCraftingBlocks {
    public static final DeferredRegister<Block> BLOCK = DeferredRegister.create(ForgeRegistries.BLOCKS, FireCrafting.MOD_ID);
    public static final RegistryObject<Block> REMINDER_BLOCK = BLOCK.register("reminder_block", () -> new AbstractGlassBlock(BlockBehaviour.Properties.of(Material.STONE).noOcclusion()){});

    public static void registry(IEventBus bus) {
        FireCrafting.getLogger().info(LoggerMarkers.MOD_INITIALIZATION, "Registry block.");
        BLOCK.register(bus);
    }
}
