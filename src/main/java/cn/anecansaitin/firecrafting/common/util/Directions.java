package cn.anecansaitin.firecrafting.common.util;

import com.google.common.collect.Lists;
import net.minecraft.core.Direction;

import java.util.List;

public class Directions {
    public static final List<Direction> DIRECTIONS = Lists.newArrayList(Direction.values());
}
