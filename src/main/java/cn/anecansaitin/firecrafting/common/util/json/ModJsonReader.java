package cn.anecansaitin.firecrafting.common.util.json;

import com.google.gson.*;
import com.ibm.icu.impl.Pair;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtUtils;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.material.Fluid;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

//json读取工具类
public class ModJsonReader {
    private static final Gson gson = new Gson();
    //读取物品集合
    public static List<ItemStack> itemsFromJson(JsonObject json, String memberName) {
        checkMember(json, memberName);
        List<ItemStack> list = new ArrayList<>();

        JsonArray items = json.getAsJsonArray(memberName);
        items.forEach((jsonElement -> {
            ItemStack itemStack = CraftingHelper.getItemStack(jsonElement.getAsJsonObject(), true, true);
            list.add(itemStack);
        }));
        return list;
    }

    //读取成分集合
    public static NonNullList<Ingredient> ingredientsFromJson(JsonObject json, String memberName) {
        checkMember(json, memberName);
        JsonArray ingredients = json.getAsJsonArray(memberName);
        NonNullList<Ingredient> nonnulllist = NonNullList.create();

        for (int i = 0; i < ingredients.size(); ++i) {
            Ingredient ingredient = Ingredient.fromJson(ingredients.get(i));
            if (!ingredient.isEmpty()) {
                nonnulllist.add(ingredient);
            }
        }

        return nonnulllist;
    }

    //读取一个方块
    public static Block blockFromJson(JsonObject json, String memberName) {
        checkMember(json, memberName);
        return ForgeRegistries.BLOCKS.getValue(new ResourceLocation(json.get(memberName).getAsString()));
    }

    //读取方块集合
    public static List<Block> blocksFromJson(JsonObject json, String memberName) {
        checkMember(json, memberName);
        List<Block> list = new ArrayList<>();
        json.getAsJsonArray(memberName).forEach((j) -> list.add(blockFromJson(j.getAsJsonObject(), "block")));
        return list;
    }

    //读取流体
    public static FluidStack fluidFromJson(JsonObject json, String memberName) {
        checkMember(json, memberName);
        JsonObject object = json.get(memberName).getAsJsonObject();
        String fluid = object.get("fluid").getAsString();
        int amount = object.get("amount").getAsInt();
        Fluid value = ForgeRegistries.FLUIDS.getValue(new ResourceLocation(fluid));
        return new FluidStack(Objects.requireNonNull(value), amount);
    }

    //读取一个实体以及nbt
    public static Pair<EntityType<?>, CompoundTag> entityAndNbtFromJson(JsonObject json, String memberName) {
        checkMember(json, memberName);
        JsonObject object = json.get(memberName).getAsJsonObject();
        EntityType<?> entity = ForgeRegistries.ENTITY_TYPES.getValue(new ResourceLocation(object.get("entity").getAsString()));
        CompoundTag nbt;

        try {
            if (object.has("nbt"))
                nbt = NbtUtils.snbtToStructure(object.get("nbt").toString());
            else
                nbt = new CompoundTag();
        } catch (CommandSyntaxException e) {
            throw new RuntimeException(e);
        }

        return Pair.of(entity, nbt);
    }

    public static List<EntityType<?>> entityTypesFromJson(JsonObject json, String memberName) {
        checkMember(json, memberName);
        ArrayList<EntityType<?>> list = new ArrayList<>();
        json.getAsJsonArray(memberName).forEach((j) -> list.add(ForgeRegistries.ENTITY_TYPES.getValue(new ResourceLocation(j.getAsJsonObject().get("entity").getAsString()))));
        return list;
    }

    public static MobEffectInstance effectFromJson(JsonObject json, String memberName) {
        checkMember(json, memberName);
        JsonObject product = GsonHelper.getAsJsonObject(json, memberName);
        MobEffect effect = ForgeRegistries.MOB_EFFECTS.getValue(new ResourceLocation(GsonHelper.getAsString(product, "effect")));
        int duration = GsonHelper.getAsInt(product, "duration");
        int amplifier = GsonHelper.getAsInt(product, "amplifier", 0);
        boolean ambient = GsonHelper.getAsBoolean(product, "ambient", false);
        boolean showIcon = GsonHelper.getAsBoolean(product, "show_icon", true);
        boolean showParticles = GsonHelper.getAsBoolean(product, "show_particles", true);
        MobEffectInstance hidden = null;

        if (product.has("hidden_effect")) {
            hidden = effectFromJson(product, "hidden_effect");
        }

        MobEffectInstance effectInstance = new MobEffectInstance(effect, duration, Math.max(amplifier, 0), ambient, showParticles, showIcon, hidden, effect.createFactorData());

        if (product.has("curative_items")) {
            ArrayList<ItemStack> curativeItems = new ArrayList<>();

            for (JsonElement items : GsonHelper.getAsJsonArray(product, "curative_items")) {
                curativeItems.add(ForgeRegistries.ITEMS.getValue(new ResourceLocation(items.getAsString())).getDefaultInstance());
            }

            effectInstance.setCurativeItems(curativeItems);
        }

        return effectInstance;
    }

    public static <T> T objectFromJson(JsonObject json, String memberName, Class<T> t) {
        checkMember(json, memberName);
        return gson.fromJson(json.get(memberName), t);
    }

    //检测成员是否存在
    public static void checkMember(JsonObject json, String memberName) {
        if (!json.has(memberName))
            throw new JsonParseException("Member not found. Name:" + memberName);
    }
}