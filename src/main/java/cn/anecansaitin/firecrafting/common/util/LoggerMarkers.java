package cn.anecansaitin.firecrafting.common.util;

import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

public class LoggerMarkers {
    public static final Marker RECIPE_SERIALIZER = MarkerManager.getMarker("RecipeSerializer");
    public static final Marker MOD_INITIALIZATION = MarkerManager.getMarker("ModInitialization");
}
