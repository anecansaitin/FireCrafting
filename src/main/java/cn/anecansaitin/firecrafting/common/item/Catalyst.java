package cn.anecansaitin.firecrafting.common.item;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.*;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class Catalyst extends Item {
    private final MutableComponent none = Component.translatable("firecrafting.item.tooltip.catalyst.base").append(Component.translatable("firecrafting.item.tooltip.catalyst.none"));
    private final MutableComponent down = Component.translatable("firecrafting.item.tooltip.catalyst.base").append(Component.translatable("firecrafting.item.tooltip.catalyst.down"));
    private final MutableComponent up = Component.translatable("firecrafting.item.tooltip.catalyst.base").append(Component.translatable("firecrafting.item.tooltip.catalyst.up"));
    private final MutableComponent east = Component.translatable("firecrafting.item.tooltip.catalyst.base").append(Component.translatable("firecrafting.item.tooltip.catalyst.east"));
    private final MutableComponent wast = Component.translatable("firecrafting.item.tooltip.catalyst.base").append(Component.translatable("firecrafting.item.tooltip.catalyst.wast"));
    private final MutableComponent south = Component.translatable("firecrafting.item.tooltip.catalyst.base").append(Component.translatable("firecrafting.item.tooltip.catalyst.south"));
    private final MutableComponent north = Component.translatable("firecrafting.item.tooltip.catalyst.base").append(Component.translatable("firecrafting.item.tooltip.catalyst.north"));

    public Catalyst() {
        super(new Properties());
    }

    @Override
    public @NotNull InteractionResultHolder<ItemStack> use(@NotNull Level world, Player player, @NotNull InteractionHand hand) {
        //右击时切换方向
        ItemStack item = player.getItemInHand(hand);
        CompoundTag root = item.getOrCreateTag();
        int orientation = root.getInt("orientation");
        root.putInt("orientation", orientation >= 5 ? -1 : orientation + 1);
        return InteractionResultHolder.success(item);
    }

    @Override
    public void appendHoverText(ItemStack stack, @Nullable Level world, @NotNull List<Component> components, @NotNull TooltipFlag isAdvanced) {
        CompoundTag root = stack.getOrCreateTag();
        int orientation = -1;

        if (root.contains("orientation")){
            orientation = root.getInt("orientation");
        }

        switch (orientation) {
            case 0 -> components.add(down);
            case 1 -> components.add(up);
            case 2 -> components.add(north);
            case 3 -> components.add(south);
            case 4 -> components.add(wast);
            case 5 -> components.add(east);
            default -> components.add(none);
        }
    }

    /**
     * @return
     * -1 -> 无
     * <br>
     * 0 -> 下方
     * <br>
     * 1 -> 上方
     * <br>
     * 2 -> 北
     * <br>
     * 3 -> 南
     * <br>
     * 4 -> 西
     * <br>
     * 5 -> 东
     */
    public static int getOrientation(ItemStack catalyst) {
        if (!(catalyst.getItem() instanceof Catalyst))
            return -1;

        CompoundTag tag = catalyst.getOrCreateTag();

        if (tag.contains("orientation")) {
            return tag.getInt("orientation");
        }

        return -1;
    }
}
