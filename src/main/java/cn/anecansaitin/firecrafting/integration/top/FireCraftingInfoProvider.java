package cn.anecansaitin.firecrafting.integration.top;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.crafting.ITimedItems;
import cn.anecansaitin.firecrafting.common.crafting.ManagerActuator;
import cn.anecansaitin.firecrafting.common.world.saveddata.FireCraftingSavedData;
import mcjty.theoneprobe.api.*;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.network.chat.contents.TranslatableContents;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseFireBlock;
import net.minecraft.world.level.block.state.BlockState;

public class FireCraftingInfoProvider implements IProbeInfoProvider {
    private final MutableComponent tick = Component.translatable("firecrafting.integration.top.fire.tick", 0);
    @Override
    public ResourceLocation getID() {
        return new ResourceLocation(FireCrafting.MOD_ID);
    }

    @Override
    public void addProbeInfo(ProbeMode probeMode, IProbeInfo iProbeInfo, Player player, Level level, BlockState blockState, IProbeHitData iProbeHitData) {
        fire(probeMode, iProbeInfo, player, level, blockState, iProbeHitData);
    }

    private void fire(ProbeMode probeMode, IProbeInfo iProbeInfo, Player player, Level level, BlockState blockState, IProbeHitData iProbeHitData) {
        if (!(blockState.getBlock() instanceof BaseFireBlock) || !(level instanceof ServerLevel)) {
            return;
        }

        ManagerActuator actuator = FireCraftingSavedData.getActuator((ServerLevel) level);
        BlockPos pos = iProbeHitData.getPos();

        if (!actuator.hasItems(pos)) {
            return;
        }

        ITimedItems items = actuator.getItems(pos);
        IProbeInfo horizontal = iProbeInfo.horizontal();
        IItemStyle style = horizontal.defaultItemStyle().width(14);

        for (ItemStack item : items.getItems()) {
            horizontal.item(item, style);
        }

        ((TranslatableContents)tick.getContents()).getArgs()[0] = items.getTime();
        iProbeInfo.mcText(tick);
    }
}
