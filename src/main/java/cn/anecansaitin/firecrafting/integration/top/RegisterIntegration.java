package cn.anecansaitin.firecrafting.integration.top;

import cn.anecansaitin.firecrafting.FireCrafting;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;

@Mod.EventBusSubscriber(modid = FireCrafting.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class RegisterIntegration {
    @SubscribeEvent
    public static void callTop(InterModEnqueueEvent event) {
        if (ModList.get().isLoaded("theoneprobe")) {
            InterModComms.sendTo("theoneprobe", "getTheOneProbe", TOPIntegration::new);
        }
    }
}
