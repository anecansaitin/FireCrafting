package cn.anecansaitin.firecrafting.integration.jei.category;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.crafting.IFireRecipe;
import cn.anecansaitin.firecrafting.client.util.RenderHelper;
import com.mojang.blaze3d.vertex.PoseStack;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.builder.IRecipeSlotBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IJeiHelpers;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.ChatFormatting;
import net.minecraft.core.NonNullList;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.block.Block;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public abstract class AbstractFireCategory<T extends IFireRecipe> implements IRecipeCategory<T> {
    protected final IJeiHelpers helpers;
    protected final Component title;
    protected final IDrawable background;
    protected static final ResourceLocation backgroundResource = new ResourceLocation(FireCrafting.MOD_ID, "textures/jei/fire_recipe_background.png");
    protected final MutableComponent fireTip = Component.translatable("firecrafting.integration.jei.tooltip.fire").withStyle(ChatFormatting.GOLD, ChatFormatting.UNDERLINE);

    public AbstractFireCategory(IJeiHelpers helpers, String title) {
        this(helpers, title, helpers.getGuiHelper().createDrawable(backgroundResource, 0, 0, 179, 85));
    }

    public AbstractFireCategory(IJeiHelpers helpers, String title, IDrawable background) {
        this.title = Component.translatable("firecrafting.integration.jei.title." + title);
        this.helpers = helpers;
        this.background = background;
    }

    protected void addInputSlots(IRecipeLayoutBuilder builder, T recipe) {
        NonNullList<Ingredient> ingredients = recipe.getIngredients();
        //原料格
        int x = 11;
        int y = 16;
        IRecipeSlotBuilder[] input = new IRecipeSlotBuilder[9];
        input[0] = builder.addSlot(RecipeIngredientRole.INPUT, x, y);
        input[1] = builder.addSlot(RecipeIngredientRole.INPUT, x + 18, y);
        input[2] = builder.addSlot(RecipeIngredientRole.INPUT, x + 2 * 18, y);
        input[3] = builder.addSlot(RecipeIngredientRole.INPUT, x, y + 18);
        input[4] = builder.addSlot(RecipeIngredientRole.INPUT, x + 18, y + 18);
        input[5] = builder.addSlot(RecipeIngredientRole.INPUT, x + 2 * 18, y + 18);
        input[6] = builder.addSlot(RecipeIngredientRole.INPUT, x, y + 2 * 18);
        input[7] = builder.addSlot(RecipeIngredientRole.INPUT, x + 18, y + 2 * 18);
        input[8] = builder.addSlot(RecipeIngredientRole.INPUT, x + 2 * 18, y + 2 * 18);
        //因为配方中的原料最多有9个，正常情况不会出现问题
        for (int i = 0; i < ingredients.size(); i++) {
            input[i].addIngredients(ingredients.get(i));
        }
    }

    protected void drawFlame(T recipe, PoseStack stack, double mouseX, double mouseY) {
        drawFlame(recipe, stack, 72, 34, mouseX, mouseY);
    }

    protected void drawFlame(T recipe, PoseStack stack, int x, int y, double mouseX, double mouseY) {
        NonNullList<Block> fires = recipe.getFires();
        RenderHelper.renderGUIBlock(fires.get(0), stack, x, y);

        if (RenderHelper.mouseOver(mouseX, mouseY, x, y, 16, 16)) {
            RenderHelper.renderSlotHighlight(stack, x, y, 16, 16, 0, -2130706433/*这是原版高亮的颜色*/);
        }
    }

    protected void addFlameTooltips(List<Component> tips, T recipe, double mouseX, double mouseY) {
        addFlameTooltips(tips, recipe, 72, 34, mouseX, mouseY);
    }

    protected void addFlameTooltips(List<Component> tips, T recipe, int x, int y, double mouseX, double mouseY) {
        if (RenderHelper.mouseOver(mouseX, mouseY, x, y, 16, 16)) {
            tips.add(fireTip);
            for (Block fire : recipe.getFires()) {
                tips.add(fire.getName());
            }
        }
    }

    @Override
    public @NotNull Component getTitle() {
        return title;
    }

    @Override
    public @NotNull IDrawable getBackground() {
        return background;
    }
}
