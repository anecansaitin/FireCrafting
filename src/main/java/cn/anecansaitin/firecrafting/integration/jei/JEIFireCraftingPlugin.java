package cn.anecansaitin.firecrafting.integration.jei;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.crafting.IFireRecipe;
import cn.anecansaitin.firecrafting.common.crafting.recipe.*;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingRecipeTypes;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingItems;
import cn.anecansaitin.firecrafting.integration.jei.category.*;
import com.google.common.collect.Lists;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.helpers.IJeiHelpers;
import mezz.jei.api.recipe.RecipeType;
import mezz.jei.api.registration.*;
import mezz.jei.api.runtime.IJeiRuntime;
import net.minecraft.client.Minecraft;
import net.minecraft.resources.ResourceLocation;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

@JeiPlugin
public class JEIFireCraftingPlugin implements IModPlugin {
    private final ResourceLocation id = new ResourceLocation(FireCrafting.MOD_ID, "jei_plugin");
    public static final RecipeType<ItemFireRecipe> ITEM_FIRE_RECIPE = RecipeType.create(FireCrafting.MOD_ID, "item_" + IFireRecipe.TYPE_ID, ItemFireRecipe.class);
    public static final RecipeType<BlockFireRecipe> BLOCK_FIRE_RECIPE = RecipeType.create(FireCrafting.MOD_ID, "block_" + IFireRecipe.TYPE_ID, BlockFireRecipe.class);
    public static final RecipeType<EntityFireRecipe> ENTITY_FIRE_RECIPE = RecipeType.create(FireCrafting.MOD_ID, "entity_" + IFireRecipe.TYPE_ID, EntityFireRecipe.class);
    public static final RecipeType<EnergyFireRecipe> ENERGY_FIRE_RECIPE = RecipeType.create(FireCrafting.MOD_ID, "energy_" + IFireRecipe.TYPE_ID, EnergyFireRecipe.class);
    public static final RecipeType<EffectFireRecipe> EFFECT_FIRE_RECIPE = RecipeType.create(FireCrafting.MOD_ID, "effect_" + IFireRecipe.TYPE_ID, EffectFireRecipe.class);


    @Override
    public @NotNull ResourceLocation getPluginUid() {
        return id;
    }

    @Override
    public void registerCategories(IRecipeCategoryRegistration registration) {
        IJeiHelpers helper = registration.getJeiHelpers();
        registration.addRecipeCategories(
                new ItemCategory(helper),
                new BlockCategory(helper),
                new EntityCategory(helper),
                new EnergyCategory(helper),
                new EffectCategory(helper));

    }

    @Override
    public void registerRecipes(@NotNull IRecipeRegistration registration) {
        List<IFireRecipe> recipes = Lists.newArrayList(Minecraft.getInstance().level.getRecipeManager().getAllRecipesFor(FireCraftingRecipeTypes.FIRE_CRAFTING.get()));
        ArrayList<ItemFireRecipe> item = new ArrayList<>();
        ArrayList<BlockFireRecipe> block = new ArrayList<>();
        ArrayList<EntityFireRecipe> entity = new ArrayList<>();
        ArrayList<EnergyFireRecipe> energy = new ArrayList<>();
        ArrayList<EffectFireRecipe> effect = new ArrayList<>();

        for (IFireRecipe recipe : recipes) {
            if (recipe instanceof ItemFireRecipe i) {
                item.add(i);
            } else if (recipe instanceof BlockFireRecipe i) {
                block.add(i);
            } else if (recipe instanceof EntityFireRecipe i) {
                entity.add(i);
            } else if (recipe instanceof EnergyFireRecipe i) {
                energy.add(i);
            } else if (recipe instanceof EffectFireRecipe i) {
                effect.add(i);
            }
        }

        registration.addRecipes(ITEM_FIRE_RECIPE, item);
        registration.addRecipes(BLOCK_FIRE_RECIPE, block);
        registration.addRecipes(ENTITY_FIRE_RECIPE, entity);
        registration.addRecipes(ENERGY_FIRE_RECIPE, energy);
        registration.addRecipes(EFFECT_FIRE_RECIPE, effect);
    }

    @Override
    public void registerRecipeCatalysts(IRecipeCatalystRegistration registration) {
        registration.addRecipeCatalyst(FireCraftingItems.BLOCK_ICON.get().getDefaultInstance(), BLOCK_FIRE_RECIPE);
        registration.addRecipeCatalyst(FireCraftingItems.ITEM_ICON.get().getDefaultInstance(), ITEM_FIRE_RECIPE);
        registration.addRecipeCatalyst(FireCraftingItems.ENERGY_ICON.get().getDefaultInstance(), ENERGY_FIRE_RECIPE);
        registration.addRecipeCatalyst(FireCraftingItems.ENTITY_ICON.get().getDefaultInstance(), ENTITY_FIRE_RECIPE);
        registration.addRecipeCatalyst(FireCraftingItems.EFFECT_ICON.get().getDefaultInstance(), EFFECT_FIRE_RECIPE);
    }

    @Override
    public void onRuntimeAvailable(IJeiRuntime jeiRuntime) {
        jeiRuntime.getIngredientManager().addIngredientsAtRuntime(VanillaTypes.ITEM_STACK,
                List.of(FireCraftingItems.ITEM_ICON.get().getDefaultInstance(),
                        FireCraftingItems.BLOCK_ICON.get().getDefaultInstance(),
                        FireCraftingItems.ENERGY_ICON.get().getDefaultInstance(),
                        FireCraftingItems.ENTITY_ICON.get().getDefaultInstance(),
                        FireCraftingItems.EFFECT_ICON.get().getDefaultInstance()));
    }
}
