package cn.anecansaitin.firecrafting.integration.jei.category;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.crafting.IFireRecipe;
import cn.anecansaitin.firecrafting.client.util.RenderHelper;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingBlocks;
import cn.anecansaitin.firecrafting.common.crafting.recipe.BlockFireRecipe;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingItems;
import com.ibm.icu.impl.Pair;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.InputConstants;
import com.mojang.blaze3d.platform.Lighting;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IJeiHelpers;
import mezz.jei.api.helpers.IModIdHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeType;
import net.minecraft.ChatFormatting;
import net.minecraft.Util;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.ImageButton;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.resources.sounds.SimpleSoundInstance;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import org.jetbrains.annotations.NotNull;
import org.joml.Math;
import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BlockCategory extends AbstractFireCategory<BlockFireRecipe> {
    public static final ResourceLocation ID = new ResourceLocation(FireCrafting.MOD_ID, "block_" + IFireRecipe.TYPE_ID);
    private final IDrawable icon;
    private final IDrawable block;
    private final MutableComponent blockTip = Component.translatable("firecrafting.integration.jei.tooltip.block").withStyle(ChatFormatting.GOLD, ChatFormatting.UNDERLINE);

    public BlockCategory(IJeiHelpers helpers) {
        super(helpers, "block");
        icon = helpers.getGuiHelper().createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(FireCraftingItems.BLOCK_ICON.get()));
        block = helpers.getGuiHelper().createDrawable(backgroundResource, 0, 85, 40, 40);
    }

    @Override
    public @NotNull IDrawable getIcon() {
        return icon;
    }

    @Override
    public @NotNull RecipeType<BlockFireRecipe> getRecipeType() {
        return new RecipeType<>(ID, BlockFireRecipe.class);
    }

    @Override
    public void setRecipe(@NotNull IRecipeLayoutBuilder builder, @NotNull BlockFireRecipe recipe, @NotNull IFocusGroup focuses) {
        addInputSlots(builder, recipe);
    }

    @Override
    public void draw(@NotNull BlockFireRecipe recipe, @NotNull IRecipeSlotsView recipeSlotsView, @NotNull PoseStack stack, double mouseX, double mouseY) {
        drawFlame(recipe, stack, mouseX, mouseY);
        block.draw(stack, 128, 22);

        if (RenderHelper.mouseOver(mouseX, mouseY, 128, 22, 40, 40)) {
            RenderHelper.renderSlotHighlight(stack, 128, 22, 40, 40, 0, -2130706433);
        }
    }

    @Override
    public @NotNull List<Component> getTooltipStrings(@NotNull BlockFireRecipe recipe, @NotNull IRecipeSlotsView recipeSlotsView, double mouseX, double mouseY) {
        ArrayList<Component> list = new ArrayList<>();
        addFlameTooltips(list, recipe, mouseX, mouseY);

        if (RenderHelper.mouseOver(mouseX, mouseY, 128, 22, 40, 40)) {
            list.add(blockTip);
            Component description = recipe.getDescription();

            if (!description.getString().isEmpty()) {
                list.add(description);
            }

            IModIdHelper idHelper = helpers.getModIdHelper();

            if (idHelper.isDisplayingModNameEnabled()) {
                //todo 考虑用回调函数实现，参考 OutputSlotTooltipCallback
                //recipe by
                String recipeId = idHelper.getFormattedModNameForModId(recipe.getId().getNamespace());
                list.add(Component.translatable("jei.tooltip.recipe.by", recipeId).withStyle(ChatFormatting.GRAY));
            }
        }
        return list;
    }

    @Override
    public boolean handleInput(@NotNull BlockFireRecipe recipe, double mouseX, double mouseY, InputConstants.Key input) {
        if (input.getValue() == InputConstants.MOUSE_BUTTON_LEFT && RenderHelper.mouseOver(mouseX, mouseY, 128, 22, 40, 40)) {
            //点击方块图标时，打开gui并播放声音
            Minecraft.getInstance().pushGuiLayer(new BlockGui(helpers, recipe));
            Minecraft.getInstance().getSoundManager().play(SimpleSoundInstance.forUI(SoundEvents.UI_BUTTON_CLICK, 1.0F));
            return true;
        } else {
            return super.handleInput(recipe, mouseX, mouseY, input);
        }
    }

    private static final class BlockGui extends Screen {
        private final BlockFireRecipe recipe;
        private final IDrawable backGroundLeft;
        private final IDrawable backGroundRight;
        private final IDrawable backGroundTop;
        private final IDrawable backGroundBottom;
        private final IDrawable product;
        private final IDrawable info;
        private final ImageButton upButton;
        private final ImageButton downButton;
        private final List<Component> infoTips = new ArrayList<>();
        private final List<Component> productTips = new ArrayList<>();
        private final int layerMax;
        private final int layerMin;
        private int layer;
        private int guiLeft = 0;
        private int guiTop = 0;
        private final int guiWidth = 230;
        private final int guiHeight = 219;
        private double translateX = 0;
        private double translateY = 0;
        private float rotationX = 0;
        private float rotationY = 0;
        private float scale = 0;

        private BlockGui(IJeiHelpers helpers, BlockFireRecipe recipe) {
            super(Component.translatable("firecrafting.integration.jei.gui.block_recipe"));
            this.recipe = recipe;
            layerMax = recipe.getProduct().get(recipe.getProduct().size() - 1).first.getY();
            layerMin = recipe.getProduct().get(0).first.getY();
            layer = layerMax;
            backGroundLeft = helpers.getGuiHelper().createDrawable(backgroundResource, 233, 0, 11, 219);
            backGroundRight = helpers.getGuiHelper().createDrawable(backgroundResource, 245, 0, 11, 219);
            backGroundTop = helpers.getGuiHelper().createDrawable(backgroundResource, 0, 141, 208, 7);
            backGroundBottom = helpers.getGuiHelper().createDrawable(backgroundResource, 0, 149, 208, 8);
            product = helpers.getGuiHelper().createDrawable(backgroundResource, 213, 126, 12, 14);
            info = helpers.getGuiHelper().createDrawable(backgroundResource, 201, 126, 12, 14);
            //这两个按钮用来设置最多渲染几层
            upButton = new ImageButton(guiLeft + 16, guiTop + 179, 11, 7, 190, 126, backgroundResource, (b) -> layer = Math.min(layerMax, layer + 1));
            upButton.setMessage(Component.translatable("firecrafting.integration.jei.gui.block_recipe.tooltip.up_layer"));
            downButton = new ImageButton(guiLeft + 16, guiTop + 199, 11, 7, 179, 126, backgroundResource, (b) -> layer = Math.max(layerMin, layer - 1));
            downButton.setMessage(Component.translatable("firecrafting.integration.jei.gui.block_recipe.tooltip.down_layer"));
            //把产物方块的信息存到tips里
            HashMap<Block, Integer> blockCounts = new HashMap<>();
            //算出每个方块各几个
            for (Pair<BlockPos, Block> pair : recipe.getProduct()) {
                Block block = pair.second;
                blockCounts.put(block, blockCounts.getOrDefault(block, 0) + 1);
            }

            for (Map.Entry<Block, Integer> entry : blockCounts.entrySet()) {
                productTips.add(Component.literal(entry.getValue() + "  ").append(entry.getKey().getName()));
            }

            infoTips.add(Component.translatable("firecrafting.integration.jei.gui.block_recipe.tooltip.info.fire"));
            infoTips.add(Component.translatable("firecrafting.integration.jei.gui.block_recipe.tooltip.info.exit"));
            infoTips.add(Component.translatable("firecrafting.integration.jei.gui.block_recipe.tooltip.info.layer"));
            infoTips.add(Component.translatable("firecrafting.integration.jei.gui.block_recipe.tooltip.info.button"));
            infoTips.add(Component.translatable("firecrafting.integration.jei.gui.block_recipe.tooltip.info.reload"));
            infoTips.add(Component.translatable("firecrafting.integration.jei.gui.block_recipe.tooltip.info.drag"));
            infoTips.add(Component.translatable("firecrafting.integration.jei.gui.block_recipe.tooltip.info.rotate"));
            infoTips.add(Component.translatable("firecrafting.integration.jei.gui.block_recipe.tooltip.info.scale"));
        }

        @Override
        protected void init() {
            guiLeft = (width - guiWidth) / 2;
            guiTop = (height - guiHeight) / 2;
            addRenderableWidget(upButton);
            addRenderableWidget(downButton);
            upButton.setX(guiLeft + 16);
            upButton.setY(guiTop + 179);
            downButton.setX(guiLeft + 16);
            downButton.setY(guiTop + 199);
        }

        @Override
        public void render(@NotNull PoseStack poseStack, int pMouseX, int pMouseY, float pPartialTick) {
            RenderSystem.disableDepthTest();
            renderBackground(poseStack);

            poseStack.pushPose();
            {
                Minecraft mc = Minecraft.getInstance();
                double scale = mc.getWindow().getGuiScale();
                RenderSystem.enableScissor((int) ((guiLeft + 11) * scale), (int) (mc.getWindow().getHeight() - (guiTop + guiHeight - 8) * scale), (int) (208 * scale), (int) (203 * scale));
                RenderSystem.enableDepthTest();
                //绘制方块
                poseStack.translate((double) width / 2 + translateX, (double) height / 2 + translateY, 0);
                poseStack.mulPoseMatrix(new Matrix4f().scale(-1, -1, -1));
                poseStack.mulPose(new Quaternionf().rotateLocalX(Math.toRadians(-30f - rotationX)));
                poseStack.mulPose(new Quaternionf().rotateLocalY(Math.toRadians(-30f - rotationY)));
                //todo 更好的方式用来提示方向
                float blockScale = 10 + this.scale;
                poseStack.scale(blockScale, blockScale, blockScale);
                MultiBufferSource.BufferSource bufferSource = mc.renderBuffers().bufferSource();
                NonNullList<Pair<BlockPos, Block>> product = recipe.getProduct();

                for (Pair<BlockPos, Block> pair : product) {
                    BlockPos pos = pair.first;

                    if (pos.getY() > layer) {
                        //用于判断需要渲染的层次
                        break;
                    }
                    pos = new BlockPos(-pos.getX(), pos.getY(), -pos.getZ());
                    renderBlock(poseStack, pair.second, pos);
                }

                //指示该位置是火焰
                renderBlock(poseStack, FireCraftingBlocks.REMINDER_BLOCK.get(), BlockPos.ZERO);
                bufferSource.endBatch();
                RenderSystem.disableDepthTest();
                RenderSystem.disableScissor();
            }
            poseStack.popPose();

            info.draw(poseStack, guiLeft + 16, guiTop + 152);
            this.product.draw(poseStack, guiLeft + 16, guiTop + 125);

            if (RenderHelper.mouseOver(pMouseX, pMouseY, guiLeft + 16, guiTop + 152, 12, 14)) {
                //info提示框
                renderComponentTooltip(poseStack, infoTips, pMouseX, pMouseY);
            } else if (RenderHelper.mouseOver(pMouseX, pMouseY, guiLeft + 16, guiTop + 125, 12, 14)) {
                //product提示框
                renderComponentTooltip(poseStack, productTips, pMouseX, pMouseY);
            }
            //这几个按钮会启用深度测试，导致有时会被渲染在方块后面
            super.render(poseStack, pMouseX, pMouseY, pPartialTick);
        }

        private void renderBlock(PoseStack poseStack, Block block, BlockPos pos) {
            MultiBufferSource.BufferSource buffersource = Minecraft.getInstance().renderBuffers().bufferSource();
            poseStack.pushPose();
            RenderSystem.enableBlend();
            RenderSystem.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            //调整光照
            BlockState blockState = block.defaultBlockState();
            int light = block.getLightEmission(blockState, Minecraft.getInstance().level, new BlockPos(1, 1, 1));

            if (light > 0) {
                Lighting.setupForFlatItems();
            } else {
                RenderSystem.setShaderLights(Util.make(new Vector3f(-0.2f, 1.2f, -0.7f), Vector3f::normalize), Util.make(new Vector3f(0.2f, -1.2f, 0.7f), Vector3f::normalize));
            }

            poseStack.translate(pos.getX(), pos.getY(), pos.getZ());
            RenderSystem.applyModelViewMatrix();
            Minecraft.getInstance().getBlockRenderer().renderSingleBlock(blockState, poseStack, buffersource, 0x00F0_00F0, OverlayTexture.NO_OVERLAY);
            poseStack.popPose();
        }

        @Override
        public void renderBackground(@NotNull PoseStack poseStack) {
            backGroundLeft.draw(poseStack, guiLeft, guiTop);
            backGroundRight.draw(poseStack, guiLeft + 219, guiTop);
            backGroundTop.draw(poseStack, guiLeft + 11, guiTop);
            backGroundBottom.draw(poseStack, guiLeft + 11, guiTop + 211);
            fill(poseStack, guiLeft + 11, guiTop + 7, guiLeft + 219, guiTop + 211, 0xFF212121);
            net.minecraftforge.common.MinecraftForge.EVENT_BUS.post(new net.minecraftforge.client.event.ScreenEvent.BackgroundRendered(this, poseStack));
        }

        //移动与旋转
        @Override
        public boolean mouseDragged(double mouseX, double mouseY, int button, double dragX, double dragY) {
            if (super.mouseDragged(mouseX, mouseY, button, dragX, dragY)) {
                return true;
            }

            if (isMouseOver(mouseX, mouseY)) {
                switch (button) {
                    case InputConstants.MOUSE_BUTTON_RIGHT -> {
                        translateX += dragX;
                        translateY += dragY;
                        return true;
                    }
                    case InputConstants.MOUSE_BUTTON_LEFT -> {
                        rotationY -= dragX;
                        rotationX += dragY;
                        return true;
                    }
                    default -> {
                        return false;
                    }
                }
            }

            return false;
        }

        //缩放
        @Override
        public boolean mouseScrolled(double mouseX, double mouseY, double delta) {
            if (super.mouseScrolled(mouseX, mouseY, delta)) {
                return true;
            }

            if (isMouseOver(mouseX, mouseY)) {
                double f = scale + delta;
                scale = (f > -9 && f < 10) ? (float) f : (Math.abs(f + 9) < Math.abs(f - 10) ? -9 : 10);
                return true;
            }

            return false;
        }

        @Override
        public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
            if (super.keyPressed(keyCode, scanCode, modifiers)) {
                return true;
            }

            switch (keyCode) {
                case InputConstants.KEY_E -> {
                    onClose();
                    return true;
                }

                case InputConstants.KEY_R -> {
                    reload();
                    return true;
                }

                case InputConstants.KEY_UP -> {
                    layer = Math.min(layerMax, layer + 1);
                    return true;
                }

                case InputConstants.KEY_DOWN -> {
                    layer = Math.max(layerMin, layer - 1);
                    return true;
                }
                default -> {
                    return false;
                }
            }
        }

        @Override
        public boolean isMouseOver(double mouseX, double mouseY) {
            return RenderHelper.mouseOver(mouseX, mouseY, guiLeft, guiTop, guiWidth, guiHeight);
        }

        @Override
        public boolean isPauseScreen() {
            return false;
        }

        private void reload() {
            translateX = 0;
            translateY = 0;
            rotationX = 0;
            rotationY = 0;
            scale = 0;
            layer = layerMax;
        }
    }
}
