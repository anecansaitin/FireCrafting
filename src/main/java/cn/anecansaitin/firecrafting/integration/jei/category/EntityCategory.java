package cn.anecansaitin.firecrafting.integration.jei.category;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.crafting.IFireRecipe;
import cn.anecansaitin.firecrafting.client.util.RenderHelper;
import cn.anecansaitin.firecrafting.common.crafting.recipe.EntityFireRecipe;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingItems;
import com.ibm.icu.impl.Pair;
import com.mojang.blaze3d.vertex.PoseStack;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IJeiHelpers;
import mezz.jei.api.helpers.IModIdHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeType;
import net.minecraft.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class EntityCategory extends AbstractFireCategory<EntityFireRecipe> {
    public static final ResourceLocation ID = new ResourceLocation(FireCrafting.MOD_ID, "entity_" + IFireRecipe.TYPE_ID);
    private final IDrawable icon;
    private final IDrawable entityBack;
    private EntityFireRecipe recipeCache = null;
    private Entity entityCache = null;

    public EntityCategory(IJeiHelpers helpers) {
        super(helpers, "entity");
        icon = helpers.getGuiHelper().createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(FireCraftingItems.ENTITY_ICON.get()));
        entityBack = helpers.getGuiHelper().createDrawable(backgroundResource, 179, 72, 45, 54);
    }

    @Override
    public @NotNull IDrawable getIcon() {
        return icon;
    }

    @Override
    public @NotNull RecipeType<EntityFireRecipe> getRecipeType() {
        return new RecipeType<>(ID, EntityFireRecipe.class);
    }

    @Override
    public void setRecipe(@NotNull IRecipeLayoutBuilder builder, @NotNull EntityFireRecipe recipe, @NotNull IFocusGroup focuses) {
        addInputSlots(builder, recipe);
    }

    @Override
    public void draw(@NotNull EntityFireRecipe recipe, @NotNull IRecipeSlotsView recipeSlotsView, @NotNull PoseStack stack, double mouseX, double mouseY) {
        drawFlame(recipe, stack, mouseX, mouseY);
        entityBack.draw(stack, 126, 15);

        if (!recipe.equals(recipeCache)) {
            recipeCache = recipe;
            Pair<EntityType<?>, CompoundTag> product = recipe.getProduct();
            entityCache = product.first.create(Minecraft.getInstance().level);
            entityCache.load(product.second);
        }

        RenderHelper.renderEntity(entityCache, stack, 126 + 22, 15 + 53, getScale(entityCache), 20, -20);

        if (RenderHelper.mouseOver(mouseX, mouseY, 127, 16, 43, 53)) {
            RenderHelper.renderSlotHighlight(stack, 127, 16, 43, 53, 0, -2130706433);
        }
    }

    private float getScale(Entity entity) {//43  59
        float width = entity.getBbWidth();
        float height = entity.getBbHeight();
        if (width <= height) {
            if (height < 0.9) return 32.9F;
            else if (height < 1) return 23.0F;
            else if (height < 1.8) return 21.7F;
            else if (height < 2) return 21.0F;
            else if (height < 3) return 15.8F;
            else if (height < 4) return 13.1F;
            else return 6.6F;
        } else {
            if (width < 1) return 27.7F;
            else if (width < 2) return 19.7F;
            else if (width < 3) return 9.5F;
            else return 6.6F;
        }
    }

    @Override
    public @NotNull List<Component> getTooltipStrings(@NotNull EntityFireRecipe recipe, @NotNull IRecipeSlotsView recipeSlotsView, double mouseX, double mouseY) {
        ArrayList<Component> list = new ArrayList<>();
        addFlameTooltips(list, recipe, mouseX, mouseY);

        if (RenderHelper.mouseOver(mouseX, mouseY, 127, 16, 43, 53)) {
            list.add(entityCache.getName());
            IModIdHelper idHelper = helpers.getModIdHelper();

            if (idHelper.isDisplayingModNameEnabled()) {
                //todo 考虑将其用IIngredientHelper实现
                //mod id
                String modId = idHelper.getFormattedModNameForModId(EntityType.getKey(entityCache.getType()).getNamespace());
                list.add(Component.translatable(modId));
                //todo 考虑用回调函数实现，参考 OutputSlotTooltipCallback
                //recipe by
                String recipeId = idHelper.getFormattedModNameForModId(recipe.getId().getNamespace());
                list.add(Component.translatable("jei.tooltip.recipe.by", recipeId).withStyle(ChatFormatting.GRAY));
            }
        }

        return list;
    }
}
