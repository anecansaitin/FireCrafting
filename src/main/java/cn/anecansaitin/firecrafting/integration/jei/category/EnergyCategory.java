package cn.anecansaitin.firecrafting.integration.jei.category;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.crafting.IFireRecipe;
import cn.anecansaitin.firecrafting.client.util.RenderHelper;
import cn.anecansaitin.firecrafting.common.crafting.recipe.EnergyFireRecipe;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingItems;
import com.mojang.blaze3d.vertex.PoseStack;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IJeiHelpers;
import mezz.jei.api.helpers.IModIdHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeType;
import net.minecraft.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.network.chat.contents.TranslatableContents;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class EnergyCategory extends AbstractFireCategory<EnergyFireRecipe> {
    public static final ResourceLocation ID = new ResourceLocation(FireCrafting.MOD_ID, "energy_" + IFireRecipe.TYPE_ID);
    private final IDrawable icon;
    private final IDrawable energyIcon;
    private final Component energy = Component.translatable("firecrafting.integration.jei.tooltip.energy").withStyle(ChatFormatting.GOLD);
    private final MutableComponent energyTotal = Component.translatable("firecrafting.integration.jei.tooltip.energy.total", 0).withStyle(ChatFormatting.LIGHT_PURPLE);
    private final MutableComponent energyPower = Component.translatable("firecrafting.integration.jei.tooltip.energy.power", 0).withStyle(ChatFormatting.LIGHT_PURPLE);
    private final MutableComponent energyDuration = Component.translatable("firecrafting.integration.jei.tooltip.energy.duration", 0).withStyle(ChatFormatting.LIGHT_PURPLE);

    public EnergyCategory(IJeiHelpers helpers) {
        super(helpers, "energy");
        icon = helpers.getGuiHelper().createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(FireCraftingItems.ENERGY_ICON.get()));
        energyIcon = helpers.getGuiHelper().createDrawable(backgroundResource, 179, 54, 10, 18);
    }

    @Override
    public @NotNull IDrawable getIcon() {
        return icon;
    }

    @Override
    public @NotNull RecipeType<EnergyFireRecipe> getRecipeType() {
        return new RecipeType<>(ID, EnergyFireRecipe.class);
    }

    @Override
    public void setRecipe(@NotNull IRecipeLayoutBuilder builder, @NotNull EnergyFireRecipe recipe, @NotNull IFocusGroup focuses) {
        addInputSlots(builder, recipe);
    }

    @Override
    public void draw(@NotNull EnergyFireRecipe recipe, @NotNull IRecipeSlotsView recipeSlotsView, @NotNull PoseStack stack, double mouseX, double mouseY) {
        drawFlame(recipe, stack, mouseX, mouseY);
        int x = 143;
        int y = 25;
        energyIcon.draw(stack, x, y);

        if (RenderHelper.mouseOver(mouseX, mouseY, 140, 22, 16, 24)) {
            RenderHelper.renderSlotHighlight(stack, 140, 22, 16, 24, 0, -2130706433);
        }

        GuiComponent.drawCenteredString(stack, Minecraft.getInstance().font, recipe.getProduct() + "FE/Tick", x + 6, y + 22, 0x000000);
        GuiComponent.drawCenteredString(stack, Minecraft.getInstance().font, recipe.getTick() + "Tick", x + 6, y + 32, 0x000000);
    }

    @Override
    public @NotNull List<Component> getTooltipStrings(@NotNull EnergyFireRecipe recipe, @NotNull IRecipeSlotsView recipeSlotsView, double mouseX, double mouseY) {
        ArrayList<Component> list = new ArrayList<>();
        addFlameTooltips(list, recipe, mouseX, mouseY);

        if (RenderHelper.mouseOver(mouseX, mouseY, 140, 22, 16, 24)) {
            list.add(energy);
            ((TranslatableContents) energyTotal.getContents()).getArgs()[0] = recipe.getTick() * recipe.getProduct();
            list.add(energyTotal);
            ((TranslatableContents) energyPower.getContents()).getArgs()[0] = recipe.getProduct();
            list.add(energyPower);
            ((TranslatableContents) energyDuration.getContents()).getArgs()[0] = recipe.getTick();
            list.add(energyDuration);
            IModIdHelper idHelper = helpers.getModIdHelper();

            if (idHelper.isDisplayingModNameEnabled()) {
                //todo 考虑用回调函数实现，参考 OutputSlotTooltipCallback
                //recipe by
                String recipeId = idHelper.getFormattedModNameForModId(recipe.getId().getNamespace());
                list.add(Component.translatable("jei.tooltip.recipe.by", recipeId).withStyle(ChatFormatting.GRAY));
            }
        }

        return list;
    }
}
