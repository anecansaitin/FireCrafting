package cn.anecansaitin.firecrafting.integration.jei.category;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.crafting.IFireRecipe;
import cn.anecansaitin.firecrafting.common.crafting.recipe.ItemFireRecipe;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingItems;
import com.mojang.blaze3d.vertex.PoseStack;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.builder.IRecipeSlotBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IJeiHelpers;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.RecipeType;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class ItemCategory extends AbstractFireCategory<ItemFireRecipe> {
    public static final ResourceLocation ID = new ResourceLocation(FireCrafting.MOD_ID, "item_" + IFireRecipe.TYPE_ID);
    private final IDrawable icon;
    private final IDrawable itemBack;

    public ItemCategory(IJeiHelpers helpers) {
        super(helpers, "item");
        icon = helpers.getGuiHelper().createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(FireCraftingItems.ITEM_ICON.get()));
        itemBack = helpers.getGuiHelper().createDrawable(backgroundResource, 179, 0, 54, 54);
    }

    @Override
    public @NotNull IDrawable getIcon() {
        return icon;
    }

    @Override
    public @NotNull RecipeType<ItemFireRecipe> getRecipeType() {
        return new RecipeType<>(ID, ItemFireRecipe.class);
    }

    @Override
    public void setRecipe(@NotNull IRecipeLayoutBuilder builder, @NotNull ItemFireRecipe recipe, @NotNull IFocusGroup focuses) {
        addInputSlots(builder, recipe);
        //产物格
        int x = 11;
        int y = 16;
        List<ItemStack> product = recipe.getProduct();
        IRecipeSlotBuilder[] output = new IRecipeSlotBuilder[9];
        output[0] = builder.addSlot(RecipeIngredientRole.OUTPUT, 18 * 3 + 57 + x, y);
        output[1] = builder.addSlot(RecipeIngredientRole.OUTPUT, 18 * 3 + 57 + x + 18, y);
        output[2] = builder.addSlot(RecipeIngredientRole.OUTPUT, 18 * 3 + 57 + x + 2 * 18, y);
        output[3] = builder.addSlot(RecipeIngredientRole.OUTPUT, 18 * 3 + 57 + x, y + 18);
        output[4] = builder.addSlot(RecipeIngredientRole.OUTPUT, 18 * 3 + 57 + x + 18, y + 18);
        output[5] = builder.addSlot(RecipeIngredientRole.OUTPUT, 18 * 3 + 57 + x + 2 * 18, y + 18);
        output[6] = builder.addSlot(RecipeIngredientRole.OUTPUT, 18 * 3 + 57 + x, y + 2 * 18);
        output[7] = builder.addSlot(RecipeIngredientRole.OUTPUT, 18 * 3 + 57 + x + 18, y + 2 * 18);
        output[8] = builder.addSlot(RecipeIngredientRole.OUTPUT, 18 * 3 + 57 + x + 2 * 18, y + 2 * 18);
        //同样的理由，产物也只会有9种
        for (int i = 0; i < product.size(); i++) {
            output[i].addItemStack(product.get(i));
        }
    }

    @Override
    public void draw(@NotNull ItemFireRecipe recipe, @NotNull IRecipeSlotsView recipeSlotsView, @NotNull PoseStack stack, double mouseX, double mouseY) {
        drawFlame(recipe, stack, mouseX, mouseY);
        itemBack.draw(stack, 121, 15);
    }

    @Override
    public @NotNull List<Component> getTooltipStrings(@NotNull ItemFireRecipe recipe, @NotNull IRecipeSlotsView recipeSlotsView, double mouseX, double mouseY) {
        ArrayList<Component> list = new ArrayList<>();
        addFlameTooltips(list, recipe, mouseX, mouseY);
        return list;
    }
}
