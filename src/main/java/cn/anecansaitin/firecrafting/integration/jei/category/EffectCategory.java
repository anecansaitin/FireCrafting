package cn.anecansaitin.firecrafting.integration.jei.category;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.api.common.crafting.IFireRecipe;
import cn.anecansaitin.firecrafting.client.util.RenderHelper;
import cn.anecansaitin.firecrafting.common.crafting.recipe.EffectFireRecipe;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingItems;
import com.mojang.blaze3d.vertex.PoseStack;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IJeiHelpers;
import mezz.jei.api.helpers.IModIdHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeType;
import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.network.chat.contents.TranslatableContents;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.item.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class EffectCategory extends AbstractFireCategory<EffectFireRecipe> {
    public static final ResourceLocation ID = new ResourceLocation(FireCrafting.MOD_ID, "effect_" + IFireRecipe.TYPE_ID);
    private final IDrawable icon;
    private final IDrawable effectIcon;
    private final MutableComponent radius = Component.translatable("firecrafting.integration.jei.tooltip.effect.radius", 0);

    public EffectCategory(IJeiHelpers helpers) {
        super(helpers, "effect");
        icon = helpers.getGuiHelper().createDrawableIngredient(VanillaTypes.ITEM_STACK, new ItemStack(FireCraftingItems.EFFECT_ICON.get()));
        effectIcon = helpers.getGuiHelper().createDrawable(backgroundResource, 189, 55, 18, 15);
    }

    @Override
    public @NotNull IDrawable getIcon() {
        return icon;
    }

    @Override
    public @NotNull RecipeType<EffectFireRecipe> getRecipeType() {
        return new RecipeType<>(ID, EffectFireRecipe.class);
    }

    @Override
    public void setRecipe(@NotNull IRecipeLayoutBuilder builder, @NotNull EffectFireRecipe recipe, @NotNull IFocusGroup focuses) {
        addInputSlots(builder, recipe);
    }

    @Override
    public void draw(@NotNull EffectFireRecipe recipe, @NotNull IRecipeSlotsView recipeSlotsView, @NotNull PoseStack stack, double mouseX, double mouseY) {
        drawFlame(recipe, stack, mouseX, mouseY);
        int x = 139;
        int y = 35;
        effectIcon.draw(stack, x, y);

        if (RenderHelper.mouseOver(mouseX, mouseY, x - 3, y - 3, 24, 21)) {
            RenderHelper.renderSlotHighlight(stack, x - 3, y - 3, 24, 21, 0, -2130706433);
        }
    }

    @Override
    public @NotNull List<Component> getTooltipStrings(@NotNull EffectFireRecipe recipe, @NotNull IRecipeSlotsView recipeSlotsView, double mouseX, double mouseY) {
        ArrayList<Component> list = new ArrayList<>();
        addFlameTooltips(list, recipe, mouseX, mouseY);
        int x = 139;
        int y = 35;

        if (RenderHelper.mouseOver(mouseX, mouseY, x - 3, y - 3, 24, 21)) {
            //等级
            MobEffectInstance effect = recipe.getEffect();
            MutableComponent effectTip = Component.translatable(effect.getDescriptionId()).withStyle(effect.getEffect().isBeneficial() ? ChatFormatting.BLUE : ChatFormatting.RED);
            list.add(effectTip);
            int lvl = effect.getAmplifier();

            if (lvl >= 1 && lvl <= 9) {
                effectTip.append(" ").append(Component.translatable("enchantment.level." + lvl + 1));
            } else if (lvl != 0){
                effectTip.append(" ").append(String.valueOf(lvl));
            }

            //时间
            int time = effect.getDuration() / 20;
            int m = time / 60;
            int s = time % 60;
            effectTip.append(" (").append(Component.literal(m + ":" + (s < 10 ? "0" + s : s))).append(")");

            //范围
            ((TranslatableContents)radius.getContents()).getArgs()[0] = recipe.getRadius();
            list.add(radius);

            IModIdHelper idHelper = helpers.getModIdHelper();

            if (idHelper.isDisplayingModNameEnabled()) {
                //todo 考虑用回调函数实现，参考 OutputSlotTooltipCallback
                //recipe by
                String recipeId = idHelper.getFormattedModNameForModId(recipe.getId().getNamespace());
                list.add(Component.translatable("jei.tooltip.recipe.by", recipeId).withStyle(ChatFormatting.GRAY));
            }
        }

        return list;
    }
}
