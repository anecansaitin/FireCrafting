package cn.anecansaitin.firecrafting.integration.jade;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ReceiveFireInfoPacket {
    private final CompoundTag tag;

    public ReceiveFireInfoPacket(CompoundTag tag) {
        this.tag = tag;
    }

    public static ReceiveFireInfoPacket read(FriendlyByteBuf buffer) {
        return new ReceiveFireInfoPacket(buffer.readNbt());
    }

    public static void write(ReceiveFireInfoPacket packet, FriendlyByteBuf buffer) {
        buffer.writeNbt(packet.tag);
    }

    public static void handler(ReceiveFireInfoPacket packet, Supplier<NetworkEvent.Context> context) {
        context.get().enqueueWork(() -> FireInfoProvider.INSTANCE.setFireInfoTag(packet.tag));
        context.get().setPacketHandled(true);
    }
}
