package cn.anecansaitin.firecrafting.integration.jade;

import cn.anecansaitin.firecrafting.FireCrafting;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingChannel;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import snownee.jade.api.*;
import snownee.jade.api.config.IPluginConfig;
import snownee.jade.api.ui.IElementHelper;

import java.util.ArrayList;
import java.util.List;

public class FireInfoProvider implements IBlockComponentProvider {
    public static final FireInfoProvider INSTANCE = new FireInfoProvider();
    private MutableComponent tickTip = Component.translatable("firecrafting.integration.jade.fire.tick").append(Component.literal(""));
    private final List<ItemStack> fireItems = new ArrayList<>();
    private long timeLastUpdate = System.currentTimeMillis();

    @Override
    public void appendTooltip(ITooltip iTooltip, BlockAccessor accessor, IPluginConfig iPluginConfig) {
        if (System.currentTimeMillis() - timeLastUpdate >= 250) {
            timeLastUpdate = System.currentTimeMillis();
            FireCraftingChannel.CHANNEL.sendToServer(new RequestFireInfoPacket(accessor.getPosition()));
        }

        if (fireItems.isEmpty()) {
            return;
        }

        IElementHelper helper = iTooltip.getElementHelper();
        iTooltip.add(helper.spacer(1, 5));

        for (ItemStack item : fireItems) {
            iTooltip.append(helper.item(item));
        }

        iTooltip.add(tickTip);
    }

    @Override
    public ResourceLocation getUid() {
        return new ResourceLocation(FireCrafting.MOD_ID, "fire");
    }

    public void setFireInfoTag(CompoundTag tag) {
        if (tag.contains("items")) {
            fireItems.clear();

            for (Tag itemTag : tag.getList("items", 10)) {
                fireItems.add(ItemStack.of((CompoundTag) itemTag));
            }
            //😡为什么只能通过新实例化一个component才能让它能实时变化，通过修改args与成员都不会变
            tickTip = tickTip.plainCopy().append(Component.literal(String.valueOf(tag.getInt("time"))));
        }
    }
}
