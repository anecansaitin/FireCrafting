package cn.anecansaitin.firecrafting.integration.jade;

import cn.anecansaitin.firecrafting.api.common.crafting.ITimedItems;
import cn.anecansaitin.firecrafting.common.crafting.ManagerActuator;
import cn.anecansaitin.firecrafting.common.registries.FireCraftingChannel;
import cn.anecansaitin.firecrafting.common.world.saveddata.FireCraftingSavedData;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.network.NetworkEvent;
import net.minecraftforge.network.PacketDistributor;

import java.util.function.Supplier;

public class RequestFireInfoPacket {
    private final BlockPos pos;

    public RequestFireInfoPacket(BlockPos pos) {
        this.pos = pos;
    }

    public static RequestFireInfoPacket read(FriendlyByteBuf buffer) {
        return new RequestFireInfoPacket(buffer.readBlockPos());
    }

    public static void write(RequestFireInfoPacket packet, FriendlyByteBuf buffer) {
        buffer.writeBlockPos(packet.pos);
    }

    public static void handler(RequestFireInfoPacket packet, Supplier<NetworkEvent.Context> context) {
        ServerPlayer player = context.get().getSender();
        ServerLevel world = player.getLevel();
        ManagerActuator actuator = FireCraftingSavedData.getActuator(world);
        CompoundTag root = new CompoundTag();
        ListTag itemsTag = new ListTag();
        int time = 0;

        if (actuator.hasItems(packet.pos)) {
            ITimedItems items = actuator.getItems(packet.pos);

            for (ItemStack item : items.getItems()) {
                itemsTag.add(item.serializeNBT());
            }

            time = items.getTime();
        }

        root.put("items", itemsTag);
        root.putInt("time", time);
        FireCraftingChannel.CHANNEL.send(PacketDistributor.PLAYER.with(() -> player), new ReceiveFireInfoPacket(root));
        context.get().setPacketHandled(true);
    }
}
