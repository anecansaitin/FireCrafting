package cn.anecansaitin.firecrafting.integration.jade;

import cn.anecansaitin.firecrafting.FireCrafting;
import net.minecraft.world.level.block.BaseFireBlock;
import snownee.jade.api.IWailaClientRegistration;
import snownee.jade.api.IWailaPlugin;
import snownee.jade.api.WailaPlugin;

@WailaPlugin(FireCrafting.MOD_ID)
public class FireCraftingJadePlugin implements IWailaPlugin {
    @Override
    public void registerClient(IWailaClientRegistration registration) {
        registration.registerBlockComponent(FireInfoProvider.INSTANCE, BaseFireBlock.class);
    }
}
