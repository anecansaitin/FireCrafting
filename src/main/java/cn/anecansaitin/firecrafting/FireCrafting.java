package cn.anecansaitin.firecrafting;

import cn.anecansaitin.firecrafting.common.registries.*;
import cn.anecansaitin.firecrafting.common.util.LoggerMarkers;
import net.minecraft.client.renderer.item.ItemProperties;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static cn.anecansaitin.firecrafting.FireCrafting.MOD_ID;

//Mod主类
@Mod(MOD_ID)
public class FireCrafting {
    public static final String MOD_ID = "firecrafting";
    private static final Logger logger = LogManager.getLogger(FireCrafting.MOD_ID);

    public FireCrafting() {
        getLogger().info(LoggerMarkers.MOD_INITIALIZATION, "Mod loading.");
        IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
        getLogger().info(LoggerMarkers.MOD_INITIALIZATION, "Setup client.");
        bus.addListener(this::clientSetup);
        getLogger().info(LoggerMarkers.MOD_INITIALIZATION, "Setup common.");
        bus.addListener(this::setup);
        FireCraftingItems.registry(bus);
        FireCraftingBlocks.registry(bus);
        FireCraftingRecipeTypes.registry(bus);
        FireCraftingSerializers.registry(bus);
        ModConfig.register();
        getLogger().info(LoggerMarkers.MOD_INITIALIZATION, "Completed.");
    }

    public void clientSetup(FMLClientSetupEvent event) {
        event.enqueueWork(() -> ItemProperties.register(FireCraftingItems.CATALYST.get(), new ResourceLocation(FireCrafting.MOD_ID, "orientation"), (item, world, entity, seed) -> item.getOrCreateTag().getInt("orientation")));
    }

    public void setup(FMLCommonSetupEvent event) {
        FireCraftingChannel.registry();
    }

    public static Logger getLogger() {
        return logger;
    }
}
